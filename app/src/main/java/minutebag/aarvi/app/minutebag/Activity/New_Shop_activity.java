package minutebag.aarvi.app.minutebag.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import minutebag.aarvi.app.minutebag.Adapter.Category_grid_adapter;
import minutebag.aarvi.app.minutebag.Adapter.TAb_PAGER_Adapter;
import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Fragment.New_dynamic_Library_frag;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class New_Shop_activity extends AppCompatActivity implements ViewPager.OnPageChangeListener, TabLayout.BaseOnTabSelectedListener {
    LinearLayout lv_newest;
    TabLayout tabLayout;
    public static ImageView iv_grid;
    Boolean flag = false;
    String view_type = "grid";
    ViewPager viewPager;
    private ArrayList<Category_List_Model> category_list;
    public static String CAT_URL;
    public static int PAGE = 1, CAT_PAGE = 1;
    public static Dialog dialog, dialog_explore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_shop_activity);
        setTitle("Shop");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Allocatememory();
        category_list = new ArrayList<>();

        if (CheckNetwork.isNetworkAvailable(New_Shop_activity.this)) {

           // CALL_ALL_CATEGORY_API();
        } else {
            Toast.makeText(New_Shop_activity.this, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        setUpViewPager();
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(this);
        tabLayout.addOnTabSelectedListener(this);


    }

    private void setUpViewPager() {
        TAb_PAGER_Adapter adapter = new TAb_PAGER_Adapter(getSupportFragmentManager());
        New_dynamic_Library_frag fView;
        for (int i = 0; i < Home_frag.category_list_models_tab.size(); i++) {
            fView = new New_dynamic_Library_frag();
            adapter.addFrag(fView, Home_frag.category_list_models_tab.get(i));
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(Home_frag.category_list_models_tab.size());

    }


    public void enableProgressBar() {
        dialog = new Dialog(New_Shop_activity.this);
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }
    public void CALL_ALL_CATEGORY_API() {

       // enableProgressBar();
        CAT_URL = Main_Url.CATEGORY_URL + "&page=" + PAGE + "&per_page=" + Main_Url.per_page
                + "&hide_empty=" + Main_Url.hide_empty;
        Log.e("url_cat_62", "" + CAT_URL);
        //  enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CAT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("url_category_173", "" + CAT_URL);
                        Log.e("response_category", "" + response);
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("jsonarry_cat", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String text = String.valueOf(Html.fromHtml(jsonObject.getString("name")));

                                        Category_List_Model gmodel = new Category_List_Model
                                                (jsonObject.getString("id")
                                                        , text);
                                        category_list.add(gmodel);


                                        String title = jsonObject.getString("name");
                                        if (title.equals("Offers") || title == "Offers") {
                                            int offerpos = i;
                                            //       Log.e("offerpos",offerpos+"");
                                        }

                                        } catch (Exception e) {
                                        Log.e("Exception", "" + e);
                                    } finally {
                                    }
                                }

                                setUpViewPager();
                                tabLayout.setupWithViewPager(viewPager);

                                PAGE = PAGE + 1;
                                CALL_ALL_CATEGORY_API();


                            } else {
                                PAGE = 1;
                                Log.e("Json_array_147", "null" + jsonArray);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(New_Shop_activity.this);
        requestQueue.add(stringRequest);

    }


    private void Allocatememory() {
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager = (ViewPager) findViewById(R.id.frameLayout);
        lv_newest = (LinearLayout) findViewById(R.id.lv_newest);
        iv_grid = (ImageView) findViewById(R.id.iv_grid);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cart, menu);
/*
        Log.e("countsubjava", "" + count1);
        MenuItem menuItem = menu.findItem(R.id.wish);
        icon = (LayerDrawable) menuItem.getIcon();

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(Sub_category.this);
        }
        // Log.e("countt",""+count);
        if (count1 == "" || count1 == null || count1 == "null" || count1.equalsIgnoreCase(null)
                || count1.equalsIgnoreCase("null")) {
            count1 = String.valueOf(0);
            Log.e("countt", "" + count1);
        }
        badge.setCount(count1);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
/*
            Log.e("checkscreen_else", "" + CheckScreen);
            onBackPressed();*/
        } else if (item.getItemId() == R.id.cart) {

            startActivity(new Intent(New_Shop_activity.this, Cart_Activity.class));
        }       // Toast.makeText(getApplicationContext(), "wishlist", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}

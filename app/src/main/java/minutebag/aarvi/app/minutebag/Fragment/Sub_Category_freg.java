package minutebag.aarvi.app.minutebag.Fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import minutebag.aarvi.app.minutebag.Activity.WrapContentLinearLayoutManager;
import minutebag.aarvi.app.minutebag.Adapter.Explore_product_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Sub_Cat_adapter;

import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.Model.Sub_Category_Model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import minutebag.aarvi.app.minutebag.Utils.MyGridView;


public class Sub_Category_freg extends Fragment {
    View v;
    public ArrayList<Sub_Category_Model> grid_model = new ArrayList<Sub_Category_Model>();
    public MyGridView gridview;
    public static Sub_Cat_adapter sub_cat_adapter;
    public static String SUB_CAT_URL;
    public static Boolean FLAG = true;
    String cat_id;

    RecyclerView recycler_subcat_explore_product;

    public static ArrayList<Shop_cat_model> product_model = new ArrayList<Shop_cat_model>();
    Explore_product_adapter explore_product_adapter;
    public static Dialog dialog, dialog_explore;

    /////////////////////

    //varible for pagination
    int page_no = 1;
    int item_count = 15;
    boolean isLoading = true;
    int pastvisibleitem, visibleitemcount, totalitemcount, previous_total = 0;
    int view_threshold = 10;
    NestedScrollView nestedscroll_subcat;

    WrapContentLinearLayoutManager layoutManager;
    ProgressBar progress_subcat_product;
    TextView tv_sub;
    public Sub_Category_freg() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sub__category_freg, container, false);

        allocatememory(v);
        grid_model.clear();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String val = bundle.getString("cat_id");
            cat_id = val;
        }
        if (CheckNetwork.isNetworkAvailable(getActivity())) {
            CALL_SUBATEGORY_GRID_API(gridview);
            CALL_EXPLORE_PRODUCT_GRID_API();
            //  CALL_EXPLORED_PRODUCT_API();
        } else {
            Toast.makeText(getActivity(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        return v;
    }

    private void CALL_SUBATEGORY_GRID_API(final GridView gridView_cat) {
        SUB_CAT_URL = Main_Url.CATEGORY_URL + "&parent=" + cat_id;
        Log.e("url_cat_62", "" + SUB_CAT_URL);
        Log.e("cat_id", "" + cat_id);
        enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SUB_CAT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_CATURL_response", "" + SUB_CAT_URL);
                        Log.e("response_subcate", "" + response);
                        try {
                            tv_sub.setVisibility(View.GONE);
                            gridView_cat.setVisibility(View.VISIBLE);

                            JSONArray jsonArray = new JSONArray(response);
                            //  progressdialog.dismiss();
                            dialog.dismiss();
                            Log.e("response_jsonarray", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                Log.e("jsonarray_105", "" + jsonArray);
                                Log.e("jsonarray_105Flag", "" + FLAG);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + jsonObject);
                                        Log.e("data", "" + jsonObject.getString("parent"));
                                        Log.e("imagesub", "" + jsonObject.getString("image"));

                                        JSONObject img_obj = null;
                                        String str = null;
                                        if (jsonObject.isNull("image")) {
                                            str="";
                                            Log.e("imagestring==>", "" + str);

                                        }else {

                                            img_obj = jsonObject.getJSONObject("image");
                                            Log.e("imageobject", "" + jsonObject.getString("image"));
                                            str = img_obj.getString("src");
                                            Log.e("imagestring==>", "" + str);

                                        }
                                        Sub_Category_Model gmodel = new Sub_Category_Model
                                                (jsonObject.getString("name"), str
                                                        , jsonObject.getString("id")
                                                        , jsonObject.getString("parent"));
                                        grid_model.add(gmodel);

                                    } catch (Exception e) {
                                        Log.e("Exception", "" + e);
                                    } finally {
                                    }
                                }

                                sub_cat_adapter = new Sub_Cat_adapter(getContext(), grid_model);
                                gridView_cat.setAdapter(sub_cat_adapter);

                                //-------call again grid api----------//
                            } else {
                                tv_sub.setVisibility(View.VISIBLE);
                                gridView_cat.setVisibility(View.GONE);
                                Log.e("Json_array_147", "null" + jsonArray);
                                Toast.makeText(getActivity(), "No Value found", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        tv_sub.setVisibility(View.VISIBLE);
                        gridView_cat.setVisibility(View.GONE);

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    private void allocatememory(View v) {
        tv_sub = (TextView) v.findViewById(R.id.tv_sub);
        gridview = (MyGridView) v.findViewById(R.id.gridview_sub_category);
        nestedscroll_subcat = (NestedScrollView) v.findViewById(R.id.nestedscroll_subcat);
        progress_subcat_product = (ProgressBar) v.findViewById(R.id.progress_subcat_product);
        recycler_subcat_explore_product = (RecyclerView) v.findViewById(R.id.recycler_subcat_explore_product);
        // gridview_explore_product = (MyGridView) v.findViewById(R.id.gridview_explore_product);
        //  frame_grid = (FrameLayout)v.findViewById(R.id.frame_grid);

        if (getActivity() != null) {
            explore_product_adapter = new Explore_product_adapter(getActivity(), product_model);
            layoutManager = new WrapContentLinearLayoutManager(getActivity(), 2);
            recycler_subcat_explore_product.setLayoutManager(layoutManager);
            recycler_subcat_explore_product.setAdapter(explore_product_adapter);
            ViewCompat.setNestedScrollingEnabled(recycler_subcat_explore_product, false);

            // ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset);
            // recycler_subcat_explore_product.addItemDecoration(itemDecoration);

        }
    }

    public void enableProgressBar() {
        dialog = new Dialog(getActivity());
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    private void CALL_EXPLORE_PRODUCT_GRID_API() {
        product_model.clear();
        progress_subcat_product.setVisibility(View.VISIBLE);
        // enable_Explore_ProgressBar();
        final String URL = Main_Url.EXPLORE_PRODUCT + "&page="
                + page_no + "&per_page=" + item_count;

        Log.e("URL_subcate_explore_474", "" + URL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_explore_474", "" + URL);
                        Log.e("response_explore", "" + response);
                        try {
                            //  dialog_explore.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("jsonArray_exp_product", "" + jsonArray);
                            // progressdialog.dismiss();
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + json_object);
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            Log.e("object_image", "" + object.getString("src"));

                                        }
                                        product_model.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        Log.e("Exception", "aaaa" + e);
                                    } finally {
                                        explore_product_adapter.notifyItemChanged(i);
                                    }
                                    progress_subcat_product.setVisibility(View.GONE);
                                }

                            } else {
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


        /*recycler_subcat_explore_product.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("is_318", "loadscroll" + dy);

                visibleitemcount=layoutManager.getChildCount();
                totalitemcount=layoutManager.getItemCount();
                pastvisibleitem=layoutManager.findFirstVisibleItemPosition();
                if(dy>0)
                {
                    Log.e("is_324", "loadscroll---" + page_no);

                    if(isLoading){
                        if(totalitemcount>previous_total)
                        {                        Log.e("is_327", "loadscroll" + page_no);

                            isLoading=false;
                            previous_total=totalitemcount;
                        }
                    }
                    if(!isLoading&&(totalitemcount-visibleitemcount)<=(pastvisibleitem+view_threshold)){

                        page_no++;
                        Log.e("isloading", "loadscroll" + page_no);

                        PerformPagination();
                        isLoading=true;
                    }
                }
            }
        });*/


        nestedscroll_subcat.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        visibleitemcount = layoutManager.getChildCount();
                        totalitemcount = layoutManager.getItemCount();
                        pastvisibleitem = layoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {

                            if ((visibleitemcount + pastvisibleitem) >= totalitemcount) {
                                page_no++;
                                Log.e("isloading", "loadscroll" + page_no);

                                PerformPagination();
                                //  isLoading=true;
//                        Load Your Data
                            }
                        }
                    }
                }
            }
        });
    }

    private void PerformPagination() {

        Log.e("URL_subcate_explore_474", "pagination");
        progress_subcat_product.setVisibility(View.VISIBLE);
        //  enable_Explore_ProgressBar();
        final String URL = Main_Url.EXPLORE_PRODUCT + "&page="
                + page_no + "&per_page=" + item_count;

        Log.e("URL_subcate_explore_474", "" + URL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_explore_474", "" + URL);
                        Log.e("response_explore", "" + response);
                        try {
                            //dialog_explore.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("jsonArray_exp_product", "" + jsonArray);
                            // progressdialog.dismiss();
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + json_object);
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            Log.e("object_image", "" + object.getString("src"));

                                        }
                                        product_model.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                      //  Toast.makeText(getActivity(), "pageloades==>" + page_no, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Log.e("Exception", "aaaa" + e);
                                    } finally {
                                        explore_product_adapter.notifyItemChanged(i);
                                    }

                                    progress_subcat_product.setVisibility(View.GONE);
                                }
                            } else {
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    private void enable_Explore_ProgressBar() {
        dialog_explore = new Dialog(getActivity());
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog_explore.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog_explore.show();
        dialog_explore.setCanceledOnTouchOutside(false);
    }

}

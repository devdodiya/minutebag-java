package minutebag.aarvi.app.minutebag.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import minutebag.aarvi.app.minutebag.Activity.EditProfile;
import minutebag.aarvi.app.minutebag.Activity.Login_activity;
import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Activity.SettingView;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

/**
 * A simple {@link Fragment} subclass.
 */
public class Settings_frag extends Fragment {
    LinearLayout lv_user_login_register,btnlogout;
    LinearLayout txtpolicy, txtrefund, txtterms, txtwebsite,txteditprofile;
    TextView txt_usernamemenu, txtuseremail;
    View v;
    public Settings_frag() {
        // Required empty public constructor
    }
    private void SetUserDetail()
    {

        String first_name = Login_preference.getfirstname(getActivity());
        String last_name = Login_preference.getLastname(getActivity());
        String email = Login_preference.getEmailid(getActivity());
        txt_usernamemenu.setText(first_name + "  " +last_name);
        txtuseremail.setText(email);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_settings_frag, container, false);
        txtpolicy = (LinearLayout) v.findViewById(R.id.txtpolicy);
        txtrefund = (LinearLayout) v.findViewById(R.id.txtrefund);
        txtterms = (LinearLayout) v.findViewById(R.id.txtterms);
        txtwebsite = (LinearLayout) v.findViewById(R.id.txtwebsite);
        txteditprofile = (LinearLayout) v.findViewById(R.id.txteditprofile);
        txt_usernamemenu = (TextView) v.findViewById(R.id.txt_usernamemenu);
        txtuseremail = (TextView) v.findViewById(R.id.txtuseremail);
        btnlogout = (LinearLayout) v.findViewById(R.id.btnlogout);
        lv_user_login_register = (LinearLayout) v.findViewById(R.id.lv_user_login_register);

        if (Login_preference.getcustomer_id(getActivity()).length() > 0)
        {
            txteditprofile.setVisibility(View.VISIBLE);
            btnlogout.setVisibility(View.VISIBLE);
            lv_user_login_register.setEnabled(false);
            SetUserDetail();
        }
        else
        {
            lv_user_login_register.setEnabled(true);
            txteditprofile.setVisibility(View.GONE);
            btnlogout.setVisibility(View.GONE);
        }
        setevent();
        return v;
        }

    private void setevent() {

        lv_user_login_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Login_activity.class));
            }
        });
        txtwebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingView.class);
                intent.putExtra("set_id", Main_Url.WEBSITE_ID);
                startActivity(intent);
            }
        });
        txtpolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingView.class);
                intent.putExtra("set_id", Main_Url.POLICY_ID);
                startActivity(intent);
            }
        });
        txtrefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingView.class);
                intent.putExtra("set_id", Main_Url.REFUND_ID);
                startActivity(intent);
            }
        });
        txtterms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingView.class);
                intent.putExtra("set_id", Main_Url.TERMS_ID);
                startActivity(intent);
            }
        });
        txteditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), EditProfile.class));

            }
        });
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Login_preference.setcustomer_id(getActivity(),"");
                Login_preference.setfirstname(getActivity(),"");
                Login_preference.setLastname(getActivity(),"");
                Login_preference.setEmailid(getActivity(),"");
                btnlogout.setVisibility(View.GONE);
                txteditprofile.setVisibility(View.GONE);
                txt_usernamemenu.setText("login and Register");
                txtuseremail.setText("Please login or create an account for free");
                lv_user_login_register.setEnabled(true);
                startActivity(new Intent(getActivity(), Navigation_activity.class));
                Toast.makeText(getActivity(),"logout successfully",Toast.LENGTH_LONG).show();
            }

        });
    }

}

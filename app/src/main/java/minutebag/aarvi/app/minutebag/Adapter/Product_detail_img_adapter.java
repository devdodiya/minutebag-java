package minutebag.aarvi.app.minutebag.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Model.Cat_Explore_Model;
import minutebag.aarvi.app.minutebag.Model.sliderimage_model;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;


    public class Product_detail_img_adapter extends RecyclerView.Adapter<Product_detail_img_adapter.MyViewHolder> {
        private List<sliderimage_model> category_models;
        private Context context;
        LayoutInflater inflater;
        public static String cat_id;
        int selectedPosition = 0;
        public static int page_no = 1;
        public int mSelectedItem = -1;

        public Product_detail_img_adapter(Context context, List<sliderimage_model> product_model) {
            this.context = context;
            this.category_models = product_model;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public Product_detail_img_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.home_category_slide_row, parent, false);
            return new Product_detail_img_adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final Product_detail_img_adapter.MyViewHolder holder, final int position) {
            final sliderimage_model model = category_models.get(position);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.drawable_ldpi_icon);
            requestOptions.error(R.drawable.drawable_ldpi_icon);
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(model.getImageurl()).into(holder.imageview_category);
            Log.e("image_adapter_59", "" + model.getImageurl());

        }


        @Override
        public int getItemCount() {
            return category_models.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageview_category;

            public MyViewHolder(View itemView) {
                super(itemView);

                imageview_category = (ImageView) itemView.findViewById(R.id.imageview_category);
            }
        }
    }







package minutebag.aarvi.app.minutebag.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import minutebag.aarvi.app.minutebag.Activity.Login_activity;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;


public class ContactUs extends Fragment {

    View v;
    EditText con_name,con_email,con_msg;
    TextView contact_msg;
    LinearLayout btnsendmail;
    public ContactUs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.fragment_contact_us, container, false);
        allocatememory();
        setevent();
        return v;
    }

    private void setevent() {
        btnsendmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String name,email,msg;
                name=con_name.getText().toString().trim();
                email=con_email.getText().toString().trim();
                msg=con_msg.getText().toString().trim();

                String CON_URL= Main_Url.CONTACT_US_URL + "&name=" + name + "&email=" + email + "&message=" + msg;

                Log.d("con_url",CON_URL);

                StringRequest request=new StringRequest(StringRequest.Method.GET, CON_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("response_con",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String status =jsonObject.getString("status");
                            if(status.equals("error")==true)
                            {
                                Log.e("error==>","response_con"+jsonObject.getString("error"));
                            }else
                            {
                                Log.e("success==>","response_con"+response.toString());

                                con_name.setText("");
                                con_email.setText("");
                                con_msg.setText("");
                                contact_msg.setVisibility(View.VISIBLE);
                                contact_msg.setText(response.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }/*
                        con_name.setText("");
                        con_email.setText("");
                        con_msg.setText("");
                        contact_msg.setVisibility(View.VISIBLE);
                        contact_msg.setText(response.toString());*/
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error",error.toString());
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(request);
            }
        });
        con_name.addTextChangedListener(new CustomerTextWatcher());
        con_email.addTextChangedListener(new CustomerTextWatcher());
        con_msg.addTextChangedListener(new CustomerTextWatcher());
    }

    private void CALL_CONTACT_API()
    {
        String name,email,msg;
        name=con_name.getText().toString().trim();
        email=con_email.getText().toString().trim();
        msg=con_msg.getText().toString().trim();


        String URL= Main_Url.CONTACT_US_URL + "&name=" + name + "&email=" + email + "&message=" + msg;

        Log.d("con_url",URL);
        StringRequest request=new StringRequest(StringRequest.Method.GET, URL,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("con_response",response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    Log.d("json_con",response);
                    String status=jsonObject.getString("status");
                    if(status.equals("error"))
                    {
                        Toast.makeText(getContext(), jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getContext(), "Email has been sent successfully", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }

    private void allocatememory()
    {
        con_name=(EditText)v.findViewById(R.id.con_name);
        con_email=(EditText)v.findViewById(R.id.con_email);
        contact_msg=(TextView) v.findViewById(R.id.contact_msg);
        con_msg=(EditText)v.findViewById(R.id.con_msg);
        btnsendmail=(LinearLayout)v.findViewById(R.id.btnsendmail);
        btnsendmail.setEnabled(false);
        btnsendmail.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    class CustomerTextWatcher implements TextWatcher
    {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable)
        {
            ValidateInput();
        }
    }

    private void ValidateInput()
    {
        if(con_name.getText().toString().trim().length()>0
                && con_email.getText().toString().trim().length()>0
                && con_msg.getText().toString().trim().length()>0)
        {
            btnsendmail.setEnabled(true);
            btnsendmail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        else
        {
            btnsendmail.setEnabled(false);
            btnsendmail.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }
}

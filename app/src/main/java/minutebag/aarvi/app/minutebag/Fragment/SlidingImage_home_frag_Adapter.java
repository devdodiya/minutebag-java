package minutebag.aarvi.app.minutebag.Fragment;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.List;

import minutebag.aarvi.app.minutebag.Model.sliderimage_model;
import minutebag.aarvi.app.minutebag.R;

/**
 * Created by ap1 on 3/5/18.
 */

class SlidingImage_home_frag_Adapter extends PagerAdapter {
    Context context;
    private List<sliderimage_model> sliderimage_models;
    private LayoutInflater inflater;
    String screen;

    public SlidingImage_home_frag_Adapter(Context context, List<sliderimage_model> sliderimage_models) {
        this.context = context;
        this.sliderimage_models = sliderimage_models;
        inflater = LayoutInflater.from(context);
        }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
    @Override
    public int getCount() {
        return sliderimage_models.size();
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ImageView image;
        Log.e("screennnnnn",""+screen);
        View view = null;

        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.home_slide_row,container,false);

        final sliderimage_model sm = sliderimage_models.get(position);

        image = (ImageView)view.findViewById(R.id.iv_home_main_banner);
        Log.e("image_url_55",""+sm.getImageurl());
        Glide.with(context).load(sm.getImageurl()).into(image);
        // Picasso.with(context).load(imageModel.getUrl()).fit().into(image);
        ((ViewPager)container).addView(view);

        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }
}
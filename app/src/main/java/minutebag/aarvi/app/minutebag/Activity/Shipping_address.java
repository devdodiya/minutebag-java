package minutebag.aarvi.app.minutebag.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import minutebag.aarvi.app.minutebag.R;

public class Shipping_address extends AppCompatActivity {
    LinearLayout lv_next_shipping;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        setTitle("Shipping");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lv_next_shipping=(LinearLayout)findViewById(R.id.lv_next_shipping);
        lv_next_shipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Shipping_address.this,Billing_address.class));
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

package minutebag.aarvi.app.minutebag.Activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Fragment.Settings_frag;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class EditProfile extends AppCompatActivity
{

    TextView txt_upfirstname,txt_uplastname,txt_uppassword;
    LinearLayout btnupdate;
    Context ctx=this;
    String firstname,lastname,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile2);
        allocatememory();
        if (Login_preference.getcustomer_id(ctx).length() > 0)
        {
            String first_name = Login_preference.getfirstname(ctx);
            String last_name = Login_preference.getLastname(ctx);
            txt_upfirstname.setText(first_name);
            txt_uplastname.setText(last_name);
        }
        setevent();
    }

    private void setevent()
    {
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EDIT_PROFILE_API();
            }
        });

    }

    private void allocatememory()
    {
        txt_upfirstname=(TextView)findViewById(R.id.txt_upfirstname);
        txt_uplastname=(TextView)findViewById(R.id.txt_uplastname);
        txt_uppassword=(TextView)findViewById(R.id.txt_uppassword);
        btnupdate=(LinearLayout)findViewById(R.id.btnupdate);
    }

    private void EDIT_PROFILE_API()
    {
        String EDIT_NEW_URL=Main_Url.EDIT_URL + Login_preference.getcustomer_id(ctx) + "?consumer_key=" + Main_Url.consumer_key + "&consumer_secret=" + Main_Url.consumer_secret;
        Log.e("edit_url", "" + EDIT_NEW_URL);

        firstname = txt_upfirstname.getText().toString().trim();
        password = txt_uppassword.getText().toString().trim();
        lastname = txt_uplastname.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, EDIT_NEW_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_update", "" + response);
                        try {
//                            {"id":1214,"date_created":"2019-02-07T16:46:45","date_created_gmt":"2019-02-07T11:16:45","date_modified":"2019-02-08T00:52:57","date_modified_gmt":"2019-02-07T19:22:57","email":"newtest@gmail.com","first_name":"test","last_name":"test","role":"customer","username":"test","billing":{"first_name":"","last_name":"","company":"","address_1":"","address_2":"","city":"","state":"","postcode":"","country":"","email":"","phone":""},"shipping":{"first_name":"","last_name":"","company":"","address_1":"","address_2":"","city":"","state":"","postcode":"","country":""},"is_paying_customer":false,"avatar_url":"https:\/\/secure.gravatar.com\/avatar\/0f9b1774f0e826e68b081b70295db47a?s=96&d=mm&r=g","meta_data":[{"id":27225,"key":"wc_last_active","value":"1549497600"}],"_links":{"self":[{"href":"https:\/\/minutebag.com\/wp-json\/wc\/v3\/customers\/1214"}],"collection":[{"href":"https:\/\/minutebag.com\/wp-json\/wc\/v3\/customers"}]}}

                            JSONObject jsonObject = new JSONObject(response);
                            if (Login_preference.getcustomer_id(ctx).equals(jsonObject.getString("id")))
                            {
                                Login_preference.setfirstname(ctx,firstname);
                                Login_preference.setLastname(ctx,lastname);
                                startActivity(new Intent(ctx,Navigation_activity.class));
                                Toast.makeText(ctx,"updated successfully",Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> KeyValuePair = new HashMap<String, String>();
               /* Log.e("username",""+username);
                Log.e("firstname",""+firstname);
                Log.e("email",""+email);
                Log.e("password",""+password);
                Log.e("lastname",""+lastname);
*/
                KeyValuePair.put("first_name",firstname);
                KeyValuePair.put("password",password);
                KeyValuePair.put("last_name", lastname);
                return KeyValuePair;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        requestQueue.add(stringRequest);

    }

}

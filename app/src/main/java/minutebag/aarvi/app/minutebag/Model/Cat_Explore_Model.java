package minutebag.aarvi.app.minutebag.Model;



public class Cat_Explore_Model {
    String cat_title, cat_image, cat_id, parent;

    public Cat_Explore_Model(String cat_title,
                             String cat_image,
                             String cat_id,
                             String parent) {
        this.cat_title = cat_title;
        this.cat_image = cat_image;
        this.cat_id = cat_id;
        this.parent = parent;
    }
    public String getCat_title() {
        return cat_title;
    }
    public void setCat_title(String cat_title) {
        this.cat_title = cat_title;
    }
    public String getCat_image() {
        return cat_image;
    }
    public void setCat_image(String cat_image) {
        this.cat_image = cat_image;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
}

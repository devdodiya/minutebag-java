package minutebag.aarvi.app.minutebag.Model;

public class sliderimage_model {

    String imageurl,type,category_image_latest;
    public sliderimage_model(String imageurl, String type, String category_image_latest) {

        this.imageurl = imageurl;
        this.type = type;
        this.category_image_latest = category_image_latest;
    }
    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory_image_latest() {
        return category_image_latest;
    }

    public void setCategory_image_latest(String category_image_latest) {
        this.category_image_latest = category_image_latest;
    }


}

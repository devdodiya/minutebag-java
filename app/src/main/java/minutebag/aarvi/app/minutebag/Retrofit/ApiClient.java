package minutebag.aarvi.app.minutebag.Retrofit;

import java.util.concurrent.TimeUnit;

import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final String BASE_URL = "https://elmizan.demoproject.info/api/";
    public static final String user_type = "lawyer";
    public static final String user_status = "1";
    private static Retrofit retrofit = null;
    public  static String  PAGE="1",PER_PAGE="10";

    static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();
    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Main_Url.sortUrl)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            }
        return retrofit;
    }
}

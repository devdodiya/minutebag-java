package minutebag.aarvi.app.minutebag.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class SettingView extends AppCompatActivity {
    WebView browser;
    TextView txtdata;
    WebView webview;
    int set_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_view);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        allocatememory();
        set_id=getIntent().getExtras().getInt("set_id");
        if (set_id==Main_Url.WEBSITE_ID)
        {   txtdata.setVisibility(View.GONE);
            webview.setVisibility(View.VISIBLE);
            webview.loadUrl("https://minutebag.com/");
            getSupportActionBar().hide();
        }
        else
        {
            CALL_API();
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
         overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        // Toast.makeText(getApplicationContext(), "wishlist", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

    private void CALL_API() {

        Log.d("policy_url",Main_Url.SETTINGS_URL);
        StringRequest request=new StringRequest(Request.Method.GET, Main_Url.SETTINGS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("json_Response",response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                  /*  browser.loadUrl("https://developer.android.com/guide/webapps/webview");
                    browser.loadUrl(jsonObject.getString("privacy_page"));*/
                  if(set_id==Main_Url.POLICY_ID)
                  {
                      setTitle("Privacy Policy");
                     // txtdata.setText(Html.fromHtml(jsonObject.getString("privacy_page")));
                      Navigation_activity.Check_String_NULL_Value(txtdata,jsonObject.getString("privacy_page"));

                  }
                  else if(set_id==Main_Url.REFUND_ID)
                  {
                      setTitle("Refund Policy");
                      Navigation_activity.Check_String_NULL_Value(txtdata,jsonObject.getString("refund_page"));

                      //txtdata.setText(Html.fromHtml(jsonObject.getString("refund_page")));
                  }
                  else if(set_id==Main_Url.TERMS_ID)
                  {
                      setTitle("Term and Services");
                      //txtdata.setText(Html.fromHtml(jsonObject.getString("terms_page")));
                      Navigation_activity.Check_String_NULL_Value(txtdata,jsonObject.getString("terms_page"));

                  }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    private void allocatememory()
    {

        txtdata =  findViewById(R.id.txtdata);
        webview =  findViewById(R.id.webview);
    }
}

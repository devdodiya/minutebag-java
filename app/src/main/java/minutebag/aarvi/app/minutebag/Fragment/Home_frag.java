package minutebag.aarvi.app.minutebag.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Activity.WrapContentLinearLayoutManager;
import minutebag.aarvi.app.minutebag.Adapter.Category_grid_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Explore_Category_Adapter;
import minutebag.aarvi.app.minutebag.Adapter.Explore_product_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Product_Latest_home_adapter;
import minutebag.aarvi.app.minutebag.Model.Cat_Explore_Model;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.Model.Slider_cat_model;
import minutebag.aarvi.app.minutebag.Model.sliderimage_model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.GPSTracker;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import minutebag.aarvi.app.minutebag.Utils.MyGridView;
import minutebag.aarvi.app.minutebag.Utils.WrapContentLinearLayot;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Home_frag extends Fragment implements View.OnClickListener {
    View v;
    public static ArrayList<Category_Model> category_grid_model = new ArrayList<Category_Model>();
    public static ArrayList<Category_List_Model> category_list_models_tab = new ArrayList<Category_List_Model>();

    public static int PAGE = 1, CAT_PAGE = 1;
    public static Category_grid_adapter category_grid_adapter;
    public static String CAT_URL;
    public static Boolean FLAG = true;

    LinearLayout lv_latest, lv_on_sale, lv_featured;
    TextView tv_latest, tv_on_sale, tv_featured;
    View view_latest, view_featured, view_on_sale;
    public static ProgressDialog progressdialog, progressdialog_home;

    ///////////////////
    private SlidingImage_home_frag_Adapter adapter;
    private Sliding_Category_image_adapter sliding_category_adapter;
    private ViewPager viewPager_featured, img_category_pager;
    private com.robohorse.pagerbullet.PagerBullet bullet;
    private List<sliderimage_model> sliderimage_models = new ArrayList<sliderimage_model>();
    private List<Slider_cat_model> slider_cat_model = new ArrayList<Slider_cat_model>();
    TextView tv_location;

    int currentPage = 0, current_category_page = 0;
    Timer timer;
    final long DELAY_MS = 500, PERIOD_MS = 3000;
    public static NestedScrollView nested_scrollView_home;

    MyGridView gridview;

    ///latest section
    String param_name;
    RecyclerView recycler_home_latest;
    Product_Latest_home_adapter product_latest_home_adapter;
    public static ArrayList<Shop_cat_model> product_model = new ArrayList<Shop_cat_model>();

    //recently viewed
    RecyclerView recycler_home_recently_viewed;

    Boolean flag_slider = false, flag_cat_grid = false, flag_ex_cat = false, flag_ex_product = false, flag_latest = false;
    //explore category
    public static ArrayList<Cat_Explore_Model> cat_model_list = new ArrayList<Cat_Explore_Model>();
    Explore_Category_Adapter explore_category_adapter;

    //explored product
    public static RecyclerView recycler_home_explored_cat, recycler_home_product;
    public static Explore_product_adapter explore_product_adapter;
    public static Context context = null;

    public static Dialog dialog, dialog_explore;
    GPSTracker gps;

    public static final int RequestPermissionCode = 7;
    public static int product_page,page=1;
    public static ProgressBar progress_explore;

    ///slider dot home top
    private CircleIndicator slider_indicator, slider_category_indicator;
    //varible for pagination

    LinearLayout lv_final_shop_click;
    public static boolean isLoading = true;
    public static int pastvisibleitem, visibleitemcount, totalitemcount, previous_total = 0;
    public static int view_threshold = 10;
    public static NestedScrollView nestedscroll_shop;

    public static WrapContentLinearLayoutManager wraplayoutManager;

    public Home_frag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home_frag, container, false);
        Navigation_activity.iv_headerlogo.setVisibility(View.VISIBLE);
        Navigation_activity.tv_title.setVisibility(View.GONE);
        context = getActivity();
//        category_list_models_tab.clear();
        progress_explore = (ProgressBar) v.findViewById(R.id.progress_explore);
        category_list_models_tab.add(new Category_List_Model("0", "All"));
        cat_model_list.clear();
        ALLOCATEMEMORY(v);
        sliderimage_models.clear();
        //////--------------------get current location---------------/////////
        if (CheckingPermissionIsEnabledOrNot()) {
           // Toast.makeText(getActivity(), "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
        } else {
            RequestMultiplePermission();
        }
        GET_CURRENT_LOCATION();

        //------------------//

        // viewPager_featured.addOnPageChangeListener(this);
        //img_category_pager.addOnPageChangeListener(this);
        //startAutoScrollViewPager_slider_featured();
        //  startAutoScrollViewPager_slider_category_latest();

        //////------------////////////
        Cat_Explore_Model model = new Cat_Explore_Model
                ("ALL", "", "0", "");
        cat_model_list.add(model);

        ///////////////////////////////////////////////////////

        if (CheckNetwork.isNetworkAvailable(getActivity())) {
            CALL_SLIDER_TOP_FEATURED_API();
            if (category_grid_model.isEmpty()) {
                Log.e("abc", "" + category_grid_model);
                CALL_CATEGORY_GRID_API(gridview);
            } else {
                Log.e("abc133", "" + category_grid_model);
                if (getActivity() != null) {
                    category_grid_adapter = new Category_grid_adapter(getContext(), category_grid_model);
                    gridview.setAdapter(category_grid_adapter);
                }
            }
            param_name = "&latest=";
            CALL_LATEST_PRODUCT_API(param_name);
            CALL_EXPLORE_CATEGORY_API();
            CALL_EXPLORED_PRODUCT_API("0",page);


        } else {
            Toast.makeText(getActivity(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
        lv_featured.setOnClickListener(this);
        lv_latest.setOnClickListener(this);
        lv_on_sale.setOnClickListener(this);


        CATEGORY_VIEW_PAGER_DATA(v);
        ViewPagerdata();

        return v;

    }


    private void CALL_EXPLORE_CATEGORY_API() {

        CAT_URL = Main_Url.CATEGORY_URL + "&page=" + CAT_PAGE + "&per_page=" + Main_Url.per_page
                + "&hide_empty=" + Main_Url.hide_empty;
        Log.e("url_cat_62", "" + CAT_URL);
        //  enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CAT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_ex_cat", "" + CAT_URL);
                        Log.e("response", "" + response);
                        try {
                            flag_ex_cat = true;
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("response_jsonarray", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                Log.e("jsonarray_105", "" + jsonArray);
                                Log.e("jsonarray_105Flag", "" + FLAG);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        if (jsonObject.getString("parent").equals("0") == true) {
                                            JSONObject image_obj = jsonObject.getJSONObject("image");
                                            Cat_Explore_Model modell = new Cat_Explore_Model
                                                    (jsonObject.getString("name"),
                                                            image_obj.getString("src")
                                                            , jsonObject.getString("id")
                                                            , jsonObject.getString("parent"));
                                            cat_model_list.add(modell);

                                        } else {
                                        }
                                    } catch (Exception e) {
                                        } finally {
                                        explore_category_adapter.notifyItemChanged(i);
                                    }
                                }


                                //-------call again grid api----------//

                                CAT_PAGE = CAT_PAGE + 1;
                                CALL_EXPLORE_CATEGORY_API();


                            } else {
                                CAT_PAGE = 1;
                                //    progressdialog.dismiss();
                                //       Log.e("flag", "flag");
                                //     Log.e("Json_array_147", "null" + jsonArray);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public static void CALL_EXPLORED_PRODUCT_API(final String catid, int page) {
        //&page=1&per_page=10&category=17
        product_page=page;

        Log.e("catid_250", "" + catid);
        Log.e("page==", "" + product_page);
        final String URL;
        if (catid.equals("0") == true || catid.equalsIgnoreCase("0") == true
                || catid == "0") {

            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_page + "&per_page=" + Main_Url.pass_per_page ;
            Log.e("urel_all", "" + URL);
        }else {
            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_page + "&per_page=" + Main_Url.pass_per_page + "&category=" + catid;
            Log.e("urel_cat", "" + URL);

        }

        product_model.clear();
        progress_explore.setVisibility(View.VISIBLE);
        enableProgressBar_explore();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("res_535_ecpl_category", "" + response);
                        try {
                            // flag_ex_product = true;
                            JSONArray jsonArray = new JSONArray(response);
                            dialog_explore.dismiss();
                            product_model.clear();
                            Log.e("jsonArray_exp_prod_home", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + json_object);
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            Log.e("object_image", "" + object.getString("src"));

                                        }
                                        product_model.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        Log.e("Exception", "aaaa" + e);
                                    } finally {
                                        explore_product_adapter.notifyItemChanged(i);
                                    }
                                }
                            } else {
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }
                            progress_explore.setVisibility(View.GONE);

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);


        ////////////////////
        nested_scrollView_home.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        visibleitemcount = wraplayoutManager.getChildCount();
                        totalitemcount = wraplayoutManager.getItemCount();
                        pastvisibleitem = wraplayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {

                            if ((visibleitemcount + pastvisibleitem) >= totalitemcount) {
                                product_page++;
                                Log.e("isloading", "loadscroll" + product_page);
                                progress_explore.setVisibility(View.VISIBLE);
                                Perform_product_Pagination(catid,product_page);
                                //  isLoading=true;
//                        Load Your Data
                            }
                        }
                    }
                }
            }
        });


    }

    public static void Perform_product_Pagination(final String catid, int page_no) {

        product_page=page_no;
        Log.e("catid_250", "" + catid);
        Log.e("page_incr", "" + page_no);
        final String URL;
        if (catid.equals("0") == true || catid.equalsIgnoreCase("0") == true
                || catid == "0") {


            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_page + "&per_page=" + Main_Url.pass_per_page ;
            Log.e("urel_all", "" + URL);
        }else {
            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_page + "&per_page=" + Main_Url.pass_per_page + "&category=" + catid;
            Log.e("urel_cat", "" + URL);

        }

        progress_explore.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("res_535_ecpl_category", "" + response);
                        try {
                            // flag_ex_product = true;
                            JSONArray jsonArray = new JSONArray(response);

                            progress_explore.setVisibility(View.GONE);

                            Log.e("jsonArray_exp_prod_home", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + json_object);
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            Log.e("object_image", "" + object.getString("src"));

                                        }
                                        product_model.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        Log.e("Exception", "aaaa" + e);
                                    } finally {
                                        explore_product_adapter.notifyItemChanged(i);
                                    }
                                }
                            } else {
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }
                            progress_explore.setVisibility(View.GONE);

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }


    public void CALL_CATEGORY_GRID_API(final GridView gridView_cat) {
        CAT_URL = Main_Url.CATEGORY_URL + "&page=" + PAGE + "&per_page=" + Main_Url.per_page
                + "&hide_empty=" + Main_Url.hide_empty;
        Log.e("url_cat_62", "" + CAT_URL);
        //  enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CAT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_CATURL_response", "" + CAT_URL);
                        Log.e("response", "" + response);
                        try {
                            flag_cat_grid = true;
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("response_jsonarray", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        if (jsonObject.getString("parent").equals("0") == true) {
                                            JSONObject image_obj = jsonObject.getJSONObject("image");
                                            Category_Model gmodel = new Category_Model
                                                    (jsonObject.getString("name"),
                                                            image_obj.getString("src")
                                                            , jsonObject.getString("id")
                                                            , jsonObject.getString("parent"));
                                            category_grid_model.add(gmodel);

                                        }
                                        String text = String.valueOf(Html.fromHtml(jsonObject.getString("name")));
                                        Category_List_Model model = new Category_List_Model
                                                (jsonObject.getString("id"), text
                                                );
                                        category_list_models_tab.add(model);

                                        String title = jsonObject.getString("name");
                                        if (title.equals("Offers") || title == "Offers") {
                                            int offerpos = i;
                                            //       Log.e("offerpos",offerpos+"");
                                        }
                                    } catch (Exception e) {
                                        //Log.e("Exception", "" + e);
                                    } finally {
                                    }
                                }

                                if (getActivity() != null) {
                                    category_grid_adapter = new Category_grid_adapter(getContext(), category_grid_model);
                                    gridView_cat.setAdapter(category_grid_adapter);
                                }

                                //-------call again grid api----------//

                                PAGE = PAGE + 1;
                                CALL_CATEGORY_GRID_API(gridView_cat);


                            } else {
                                PAGE = 1;
                                Log.e("flag", "flag");
                                Log.e("Json_array_147", "null" + jsonArray);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }
    private void CALL_LATEST_PRODUCT_API(String param_name) {
        //Log.e("URL_latest_474", "");
        Log.e("param_name_698", "" + param_name);
        enableProgressBar();
        final String PRODUCT_URL = Main_Url.PRODUCT_ONSALE_URL + param_name + "true" + "&per_page" + "10";
        //Log.e("URL_latest_474", "" + PRODUCT_URL);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, PRODUCT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_latest_474", "" + PRODUCT_URL);
                        Log.e("res_latest_723", "" + response);
                        try {
                            flag_latest = true;
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("jsonArray_723_product", "" + jsonArray);
                            dialog.dismiss();
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object_latest", "" + json_object);

                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            //Log.e("object_image", "" + object.getString("src"));

                                        }
                                        product_model.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        //Log.e("Exception", "aaaa" + e);
                                    } finally {
                                        product_latest_home_adapter.notifyItemChanged(i);
                                    }
                                }


                            } else {
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }

    public void enableProgressBar() {
        dialog = new Dialog(getActivity());
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }
    public static void enableProgressBar_explore() {
        dialog_explore = new Dialog(context);
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog_explore.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog_explore.show();
        dialog_explore.setCanceledOnTouchOutside(false);
    }
    private void ALLOCATEMEMORY(View v) {
        slider_indicator = (CircleIndicator) v.findViewById(R.id.slider_indicator);
        slider_category_indicator = (CircleIndicator) v.findViewById(R.id.slider_category_indicator);
        viewPager_featured = (ViewPager) v.findViewById(R.id.imgpager);
        img_category_pager = (ViewPager) v.findViewById(R.id.img_category_pager);

        view_latest = (View) v.findViewById(R.id.view_latest);
        view_featured = (View) v.findViewById(R.id.view_featured);
        view_on_sale = (View) v.findViewById(R.id.view_on_sale);
        gridview = (MyGridView) v.findViewById(R.id.gridview_cat_home);
        lv_latest = (LinearLayout) v.findViewById(R.id.lv_latest);
        lv_on_sale = (LinearLayout) v.findViewById(R.id.lv_on_sale);
        lv_featured = (LinearLayout) v.findViewById(R.id.lv_featured);
        tv_latest = (TextView) v.findViewById(R.id.tv_latest);
        tv_on_sale = (TextView) v.findViewById(R.id.tv_on_sale);
        tv_featured = (TextView) v.findViewById(R.id.tv_featured);
        tv_location = (TextView) v.findViewById(R.id.tv_location);
        nested_scrollView_home = (NestedScrollView) v.findViewById(R.id.scrollview_home);
        nested_scrollView_home.smoothScrollTo(0, 0);

        ///set animation
      /*  int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        gridview.setLayoutAnimation(animation);
*/

        //latest product

        Log.e("product_model_906", "" + product_model);
        recycler_home_latest = (RecyclerView) v.findViewById(R.id.recycler_home_latest);
        product_latest_home_adapter = new Product_Latest_home_adapter(getActivity(), product_model);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_home_latest.setLayoutManager(layoutManager);
        recycler_home_latest.setAdapter(product_latest_home_adapter);


      /*  //recently viewed product
        recycler_home_recently_viewed=(RecyclerView)v.findViewById(R.id.recycler_home_recently_viewed);
        product_latest_home_adapter = new Product_Latest_home_adapter(getActivity(), product_model);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_home_recently_viewed.setLayoutManager(layoutManager1);
        recycler_home_recently_viewed.setAdapter(product_latest_home_adapter);
*/
        //explored category
        recycler_home_explored_cat = (RecyclerView) v.findViewById(R.id.recycler_home_explored_cat);
        explore_category_adapter = new Explore_Category_Adapter(getActivity(), cat_model_list);
        // LinearLayoutManager layoutManager3 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_home_explored_cat.setLayoutManager(new WrapContentLinearLayot(getActivity(), LinearLayout.HORIZONTAL, false));
        recycler_home_explored_cat.setAdapter(explore_category_adapter);


        //////explored product
        recycler_home_product = (RecyclerView) v.findViewById(R.id.recycler_home_product);
        Log.e("761_product", "dfhfgnhd");
        if (getActivity() != null) {
            Log.e("761_product", "nljnljhkljglyg");
            explore_product_adapter = new Explore_product_adapter(getActivity(), product_model);
            wraplayoutManager = new WrapContentLinearLayoutManager(getActivity(), 2);
            recycler_home_product.setLayoutManager(wraplayoutManager);
            recycler_home_product.setAdapter(explore_product_adapter);
            recycler_home_product.setNestedScrollingEnabled(false);
            // ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset);
            // recycler_subcat_explore_product.addItemDecoration(itemDecoration);

        }
    }
    @Override
    public void onClick(View view) {
        if (view == lv_featured) {
            param_name = "&featured=";

            tv_featured.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            tv_on_sale.setTextColor(getResources().getColor(R.color.black));
            tv_latest.setTextColor(getResources().getColor(R.color.black));

            view_featured.setVisibility(View.VISIBLE);
            view_on_sale.setVisibility(View.GONE);
            view_latest.setVisibility(View.GONE);

            if (CheckNetwork.isNetworkAvailable(getActivity())) {
                CALL_LATEST_PRODUCT_API(param_name);
            } else {
                Toast.makeText(getActivity(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }


        } else if (view == lv_latest) {

            param_name = "&latest=";
            view_featured.setVisibility(View.GONE);
            view_on_sale.setVisibility(View.GONE);
            view_latest.setVisibility(View.VISIBLE);

            tv_latest.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            tv_featured.setTextColor(getResources().getColor(R.color.black));
            tv_on_sale.setTextColor(getResources().getColor(R.color.black));
            if (CheckNetwork.isNetworkAvailable(getActivity())) {
                CALL_LATEST_PRODUCT_API(param_name);
            } else {
                Toast.makeText(getActivity(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        } else if (view == lv_on_sale) {
            param_name = "&on_sale=";

            view_featured.setVisibility(View.GONE);
            view_on_sale.setVisibility(View.VISIBLE);
            view_latest.setVisibility(View.GONE);

            tv_on_sale.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            tv_latest.setTextColor(getResources().getColor(R.color.black));
            tv_featured.setTextColor(getResources().getColor(R.color.black));

            if (CheckNetwork.isNetworkAvailable(getActivity())) {

                CALL_LATEST_PRODUCT_API(param_name);
            } else {
                Toast.makeText(getActivity(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }


    //image slider api
    private void CATEGORY_VIEW_PAGER_DATA(View v) {
        sliding_category_adapter = new Sliding_Category_image_adapter(getActivity(), slider_cat_model);
        img_category_pager.setAdapter(sliding_category_adapter);

        // sliding_category_adapter.notifyDataSetChanged();
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                if (current_category_page == sliderimage_models.size() - 1) {
                    current_category_page = 0;
                }
                img_category_pager.setCurrentItem(current_category_page++, true);
            }
        };


        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    private void ViewPagerdata() {

        adapter = new SlidingImage_home_frag_Adapter(getActivity(), sliderimage_models);
        viewPager_featured.setAdapter(adapter);
        // adapter.notifyDataSetChanged();

        // slider_indicator.setViewPager(viewPager_featured);
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == sliderimage_models.size() - 1) {
                    currentPage = 0;
                }
                viewPager_featured.setCurrentItem(currentPage++, true);
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

    }

    private void CALL_SLIDER_TOP_FEATURED_API() {
        sliderimage_models.clear();
        slider_cat_model.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Main_Url.HOME_BANNERURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_banner_400", "" + response);
                        Log.e("banner_url", "" + Main_Url.HOME_BANNERURL);
                        try {
                            sliderimage_models.clear();
                            slider_cat_model.clear();


                            flag_slider = true;
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray data_arr = jsonObject.getJSONArray("data");
                            if (data_arr != null && data_arr.isNull(0) != true) {
                                for (int i = 0; i < data_arr.length(); i++) {
                                    try {
                                        JSONObject data_object = data_arr.getJSONObject(i);
                                        Log.e("type_402", "" + data_object.getString("type"));
                                       /* if (data_object.getString("type").equals("featured") == true) {
                                            Log.e("imgurl_439", "" + data_object.getString("banners_image"));

                                            sliderimage_model imageModel = new sliderimage_model(data_object.getString("banners_image")
                                                    , data_object.getString("type"), "");
                                            sliderimage_models.add(imageModel);
                                        }*/

                                        if (data_object.getString("type").equals("on_sale") == true) {
                                            Log.e("imgurl_445", "" + data_object.getString("banners_image"));
                                            Slider_cat_model cat_imageModel = new Slider_cat_model(data_object.getString("banners_id")
                                                    , data_object.getString("banners_image")
                                                    , data_object.getString("type"), data_object.getString("banners_title"));
                                            slider_cat_model.add(cat_imageModel);
                                        }
                                        sliderimage_model imageModel = new sliderimage_model(data_object.getString("banners_image")
                                                , data_object.getString("type"), "");
                                        sliderimage_models.add(imageModel);


                                    } catch (Exception e) {
                                        //           Log.e("Exception", "" + e);
                                    } finally {
                                        adapter.notifyDataSetChanged();
                                        sliding_category_adapter.notifyDataSetChanged();
                                    }
                                }
                                slider_category_indicator.setViewPager(img_category_pager);
                                slider_indicator.setViewPager(viewPager_featured);

                            } else {
                                Log.e("dataarray", "" + data_arr);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Toast.makeText(getActivity()), "not get Response", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> KeyValuePair = new HashMap<String, String>();
                return KeyValuePair;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void CALL_SLIDER_LATEST_API() {
        sliderimage_models.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Main_Url.HOME_BANNERURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_banner_400", "" + response);
                        Log.e("banner_url_onsale", "" + Main_Url.HOME_BANNERURL);
                        try {
                            sliderimage_models.clear();
                            flag_slider = true;
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray data_arr = jsonObject.getJSONArray("data");
                            if (data_arr != null && data_arr.isNull(0) != true) {
                                for (int i = 0; i < data_arr.length(); i++) {
                                    try {
                                        JSONObject data_object = data_arr.getJSONObject(i);
                                        Log.e("type_on_sale", "" + data_object.getString("type"));
                                        Log.e("banners_image_latest", "" + data_object.getString("banners_image"));
                                        sliderimage_model imageModel = new sliderimage_model(data_object.getString("banners_image")
                                                , data_object.getString("type"), data_object.getString("banners_image"));
                                        sliderimage_models.add(imageModel);
                                        // img_category_pager.setAdapter(new Sliding_Category_image_adapter(getActivity(), sliderimage_models));

                                      /*  if (data_object.getString("type").equals("latest") == true) {

                                            sliderimage_model imageModel = new sliderimage_model(data_object.getString("banners_image")
                                                    , data_object.getString("type"), "");
                                            sliderimage_models.add(imageModel);
                                            img_category_pager.setAdapter(new Sliding_Category_image_adapter(getActivity(), sliderimage_models));

                                        }else {
                                            sliderimage_model imageModel = new sliderimage_model(data_object.getString("banners_image")
                                                    , data_object.getString("type"), "");
                                            sliderimage_models.add(imageModel);
                                            img_category_pager.setAdapter(new Sliding_Category_image_adapter(getActivity(), sliderimage_models));

                                        }*/

                                    } catch (Exception e) {
                                        //           Log.e("Exception", "" + e);
                                    } finally {
                                    }
                                }
                                slider_category_indicator.setViewPager(img_category_pager);
                            } else {
                                Log.e("dataarray", "" + data_arr);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Toast.makeText(getActivity()), "not get Response", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> KeyValuePair = new HashMap<String, String>();
                return KeyValuePair;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


    //Permission function starts from here
    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(getActivity(), new String[]
                {ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION
                }, RequestPermissionCode);

    }

    // Calling override method.
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean LocationPermission = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    boolean ACCESS_COARSE_LOCATION_permisssion = grantResults[6] == PackageManager.PERMISSION_GRANTED;

                    if (LocationPermission && ACCESS_COARSE_LOCATION_permisssion) {

                        Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    // Checking permission is enabled or not using function starts from here.
    public boolean CheckingPermissionIsEnabledOrNot() {
        int ACCESS_FINE_LOCATION_PermissionResult = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int ACCESS_COARSE_LOCATION_PermissionResult = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);

        return ACCESS_FINE_LOCATION_PermissionResult == PackageManager.PERMISSION_GRANTED &&
                ACCESS_COARSE_LOCATION_PermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    //Get current location
    private void GET_CURRENT_LOCATION() {
        gps = new GPSTracker(getActivity());

        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Log.e("latitude", "" + latitude);
            Log.e("longitude", "" + longitude);

            try {
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                Log.e("shaher_vinod", "" + addresses.get(0).getLocality() + "," + addresses.get(0).getCountryName());

                String live_location = addresses.get(0).getAddressLine(0);
                Log.e("live_location", "" + live_location);

                tv_location.setText("Your Location :" + " " + live_location);

            } catch (Exception e) {

            }

        } else {
            gps.showSettingsAlert();
        }

    }


}

package minutebag.aarvi.app.minutebag.Adapter;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Activity.Product_Detail;
import minutebag.aarvi.app.minutebag.Model.AddToCart;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;


public class Shop_list_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    String Package_id;
    private List<Shop_cat_model> product_model;
    String old_price, new_price;
    int product_quantity = 0;
    int choice = 0;
    public Context ctx;
    LayoutInflater inflater;
    public String type = "package", user_id, op, URL_wishlist;
    public  static ArrayList<AddToCart> cartList = new ArrayList<>();

    public Shop_list_adapter(Context context, List<Shop_cat_model> model) {
        this.ctx = context;
        this.product_model = model;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        choice = Main_Url.GRID_TYPE;

        switch (choice) {
            case 0:
                View itemView = inflater.inflate(R.layout.product_row, parent, false);
                return new Shop_list_adapter.MyViewHolder(itemView);
            case 2:
                View listitem = inflater.inflate(R.layout.shop_vertical_list_row, parent, false);
                return new Shop_list_adapter.MyNewHolder(listitem);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Shop_cat_model model = product_model.get(position);

        switch (Main_Url.GRID_TYPE) {
            case 2:
                Log.d("list", Main_Url.GRID_TYPE + "");
                final MyNewHolder listHolder = (MyNewHolder) holder;
                // listHolder.tv_product_title.setText(model.getProduct_name());
                // listHolder.tv_new_price.setText(ctx.getResources().getString(R.string.rs) + model.getProduct_new_price());
                // listHolder.tv_old_price.setText(ctx.getResources().getString(R.string.rs) + model.getProduct_old_price());

                Navigation_activity.Check_String_NULL_Value(listHolder.tv_product_title, model.getProduct_name());
                Navigation_activity.Check_String_NULL_Value(listHolder.tv_new_price, ctx.getResources().getString(R.string.rs) + model.getProduct_new_price());
                Navigation_activity.Check_String_NULL_Value(listHolder.tv_old_price, ctx.getResources().getString(R.string.rs) + model.getProduct_old_price());
                listHolder.tv_old_price.setPaintFlags(listHolder.tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                double oldprice, newprice;
                //  Log.e("old_price=", "" + model.getProduct_old_price());
                //  Log.e("new_price=", "" + model.getProduct_new_price());

                old_price = model.getProduct_old_price();
                new_price = model.getProduct_new_price();
                if (old_price.equals("") == true) {
                    old_price = "0";
                }
                if (new_price.equals("") == true) {
                    new_price = "0";
                }

                if (model.getProduct_old_price().equals("") != true || model.getProduct_new_price().equals("") != true) {


                    oldprice = Double.parseDouble(old_price);
                    newprice = Double.parseDouble(new_price);
                    double save_rs = oldprice - newprice;
                    int res = (int) save_rs;
                    String result = String.valueOf(res);
                    //   listHolder.tv_save_rs.setText("SAVE RS." + result);
                } else {
                    listHolder.tv_save_rs.setVisibility(View.GONE);
                }


           /*     listHolder.lv_variable_spinner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

    dialog.setContentView(R.layout.custom_dialog_shop);
                dialog.show();

                        Dialog dialog = new Dialog(ctx);
                        AppCompatActivity activity = (AppCompatActivity) view.getContext();
                        Rect displayRectangle = new Rect();
                        Window window = activity.getWindow();
                        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

// inflate and adjust layout
                        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.custom_dialog_shop, null);
                        layout.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
                        layout.setMinimumHeight((int) (displayRectangle.height() * 0.4f));
                        dialog.setContentView(layout);
                        dialog.show();
                    }
                });*/
                listHolder.lv_add_to_cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        listHolder.lv_add_to_cart.setVisibility(View.GONE);
                        listHolder.lv_add_quantity.setVisibility(View.VISIBLE);
                        listHolder.lv_increse.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                model.setIncrease(model.getIncrease() + 1);
                                Log.e("product_104", "" + model.getIncrease());
                                String result = String.valueOf(model.getIncrease());
                                listHolder.tv_product_quantity.setText(result);
                            }
                        });
                        listHolder.lv_decrese.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (model.getIncrease() != 0) {
                                    model.setIncrease(model.getIncrease() - 1);
                                    String result = String.valueOf(model.getIncrease());
                                    Log.e("product_1011_minus", "" + model.getIncrease());
                                    listHolder.tv_product_quantity.setText(result);
                                } else {
                                    listHolder.tv_product_quantity.setText(String.valueOf(model.getIncrease()));
                                }
                            }
                        });
                    }
                });

                listHolder.lv_product_click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AppCompatActivity activity = (AppCompatActivity) view.getContext();
                        //Bundle id_bundle=new Bundle();
                        //id_bundle.putString("cat_id",model.getCat_id());
                        //Fragment myFragment = new Sub_Category_freg();
                        // myFragment.setArguments(id_bundle);
                        Intent i = new Intent(ctx, Product_Detail.class);
                        //  Log.e("product_id_12", "" + model.getProduct_id());

                        i.putExtra("product_id", model.getProduct_id());
                        activity.startActivity(i);

                        // activity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, myFragment).addToBackStack(null).commit();


                    }
                });
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.drawable_ldpi_icon);
                requestOptions.error(R.drawable.drawable_ldpi_icon);
                Glide.with(ctx)
                        .setDefaultRequestOptions(requestOptions)
                        .load(model.getProduct_img()).into(listHolder.iv_product);

                // Glide.with(ctx).load(model.getProduct_img()).into(listHolder.iv_product);

                break;

            case 0:
                Log.d("grid", Main_Url.GRID_TYPE + "");
                final MyViewHolder gridholder = (MyViewHolder) holder;

                // gridholder.tv_product_title.setText(model.getProduct_name());
                // gridholder.tv_new_price.setText(ctx.getResources().getString(R.string.rs) + model.getProduct_new_price());
                //gridholder.tv_old_price.setText(ctx.getResources().getString(R.string.rs) + model.getProduct_old_price());

                Navigation_activity.Check_String_NULL_Value(gridholder.tv_product_title, model.getProduct_name());
                Navigation_activity.Check_String_NULL_Value(gridholder.tv_new_price, ctx.getResources().getString(R.string.rs) + model.getProduct_new_price());
                Navigation_activity.Check_String_NULL_Value(gridholder.tv_old_price, ctx.getResources().getString(R.string.rs) + model.getProduct_old_price());
                gridholder.tv_old_price.setPaintFlags(gridholder.tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                double oldprice1, newprice1;
                // Log.e("old_price=", "" + model.getProduct_old_price());
                // Log.e("new_price=", "" + model.getProduct_new_price());

                old_price = model.getProduct_old_price();
                new_price = model.getProduct_new_price();
                if (old_price.equals("") == true) {
                    old_price = "0";
                }
                if (new_price.equals("") == true) {
                    new_price = "0";
                }

                if (model.getProduct_old_price().equals("") != true || model.getProduct_new_price().equals("") != true) {

                    oldprice1 = Double.parseDouble(old_price);
                    newprice1 = Double.parseDouble(new_price);
                    double save_rs1 = oldprice1 - newprice1;
                    int res1 = (int) save_rs1;
                    String result1 = String.valueOf(res1);
//                gridholder.tv_save_rs.setText("SAVE RS." + result1);
                } else {
                    //     gridholder.tv_save_rs.setVisibility(View.GONE);
                }

                gridholder.lv_variable_spinner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog dialog = new Dialog(ctx);
                        dialog.setContentView(R.layout.custom_dialog_shop);
                        dialog.show();

                        AppCompatActivity activity = (AppCompatActivity) view.getContext();
                        Rect displayRectangle = new Rect();
                        Window window = activity.getWindow();
                        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

// inflate and adjust layout
                        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.custom_dialog_shop, null);
                        layout.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
                        layout.setMinimumHeight((int) (displayRectangle.height() * 0.4f));
                        dialog.setContentView(layout);
                        dialog.show();
                    }
                });
                gridholder.lv_add_to_cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        model.setIncrease(1);
                        String total= String.valueOf(model.getIncrease() * Double.parseDouble(model.getProduct_new_price()));
                        final AddToCart cartItem=new AddToCart(model.getProduct_old_price(),
                                                        model.getProduct_new_price(),
                                                        model.getProduct_name(),
                                                        model.getProduct_img(),
                                                        model.getIncrease(),
                                                        model.getProduct_id(),
                                                         total);
                      /*  AddToCart selectedCart=SearchItemUsingId(cartList,model.getProduct_id());
                        if (selectedCart!=null)
                        {
                            cartList.add(cartItem);
                        }
                        else {
                            selectedCart.setIncrease(Integer.parseInt(gridholder.tv_product_quantity.getText().toString())) ;
                             total= String.valueOf(selectedCart.getIncrease()*Double.parseDouble( selectedCart.getNewPrice()));
                            selectedCart.setTotal(total);
                        }*/
                        cartList.add(cartItem);
                        gridholder.lv_add_to_cart.setVisibility(View.GONE);
                        gridholder.lv_add_quantity.setVisibility(View.VISIBLE);
                        gridholder.lv_increse.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                model.setIncrease(model.getIncrease() + 1);
                                Log.e("product_104", "" + model.getIncrease());
                                String result = String.valueOf(model.getIncrease());
                                gridholder.tv_product_quantity.setText(result);
                                cartItem.setIncrease(model.getIncrease());
                                String total= String.valueOf(model.getIncrease() * Double.parseDouble(model.getProduct_new_price()));

                                cartItem.setTotal(total);
                               /* Log.e("name",cartItem.getProductName());
                                Log.e("oldprice",cartItem.getOldPrice());
                                Log.e("newprice",cartItem.getNewPrice());
                                Log.e("increase",cartItem.getIncrease()+"");
                                Log.e("total",cartItem.getTotal()+"");*/
                            }
                        });
                        gridholder.lv_decrese.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (model.getIncrease() != 0) {
                                    model.setIncrease(model.getIncrease() - 1);
                                    String result = String.valueOf(model.getIncrease());
                                    Log.e("product_1011_minus", "" + model.getIncrease());
                                    gridholder.tv_product_quantity.setText(result);
                                    cartItem.setIncrease(model.getIncrease());
                                    String total= String.valueOf(model.getIncrease() * Double.parseDouble(model.getProduct_new_price()));
                 cartItem.setTotal(total);

                                   /* Log.e("name",cartItem.getProductName());
                                    Log.e("oldprice",cartItem.getOldPrice());
                                    Log.e("newprice",cartItem.getNewPrice());
                                    Log.e("increase",cartItem.getIncrease()+"");
                                    Log.e("total",cartItem.getTotal()+"");*/
                                } else {
                                    gridholder.tv_product_quantity.setText(String.valueOf(model.getIncrease()));
                                }
                            }
                        });
                    }
                });

                gridholder.lv_product_click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AppCompatActivity activity = (AppCompatActivity) view.getContext();
                        //Bundle id_bundle=new Bundle();
                        //id_bundle.putString("cat_id",model.getCat_id());
                        //Fragment myFragment = new Sub_Category_freg();
                        // myFragment.setArguments(id_bundle);
                        Intent i = new Intent(ctx, Product_Detail.class);
                        //   Log.e("product_id_12", "" + model.getProduct_id());

                        i.putExtra("product_id", model.getProduct_id());
                        activity.startActivity(i);

                        // activity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, myFragment).addToBackStack(null).commit();


                    }
                });

                RequestOptions requestOptions1 = new RequestOptions();
                requestOptions1.placeholder(R.drawable.drawable_ldpi_icon);
                requestOptions1.error(R.drawable.drawable_ldpi_icon);
                Glide.with(ctx)
                        .setDefaultRequestOptions(requestOptions1)
                        .load(model.getProduct_img()).into(gridholder.iv_product);

                // Glide.with(ctx).load(model.getProduct_img()).into(gridholder.iv_product);

                break;
        }


    }

    private AddToCart SearchItemUsingId(ArrayList<AddToCart> cartList, String product_id) {
        AddToCart selectedCart=null;
        for(AddToCart cart : cartList) {

            if(cart.getId().equals(product_id)){
                //found it!
               selectedCart=cart;
            }
        }
        return selectedCart;
    }

    @Override
    public int getItemCount() {
        return product_model.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_product, iv_wishlist;
        LinearLayout lv_product_click, lv_add_to_cart, lv_variable_spinner, lv_decrese, lv_increse, lv_add_quantity;
        TextView tv_add_to_cart, tv_save_rs, tv_product_title, tv_old_price, tv_new_price, tv_product_quantity, tv_minus, tv_add;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_product = (ImageView) itemView.findViewById(R.id.iv_product);
            iv_wishlist = (ImageView) itemView.findViewById(R.id.iv_wishlist);

            lv_product_click = (LinearLayout) itemView.findViewById(R.id.lv_product_click);
            lv_add_to_cart = (LinearLayout) itemView.findViewById(R.id.lv_add_to_cart);
            lv_decrese = (LinearLayout) itemView.findViewById(R.id.lv_decrese);
            lv_increse = (LinearLayout) itemView.findViewById(R.id.lv_increse);
            lv_add_quantity = (LinearLayout) itemView.findViewById(R.id.lv_add_quantity);
            lv_variable_spinner = (LinearLayout) itemView.findViewById(R.id.lv_variable_spinner);

            tv_add_to_cart = (TextView) itemView.findViewById(R.id.tv_add_to_cart);
            tv_add = (TextView) itemView.findViewById(R.id.tv_add);
            tv_minus = (TextView) itemView.findViewById(R.id.tv_minus);
            tv_product_quantity = (TextView) itemView.findViewById(R.id.tv_product_quantity);
            tv_save_rs = (TextView) itemView.findViewById(R.id.tv_save_rs);
            tv_product_title = (TextView) itemView.findViewById(R.id.tv_product_title);
            tv_old_price = (TextView) itemView.findViewById(R.id.tv_old_price);
            tv_new_price = (TextView) itemView.findViewById(R.id.tv_new_price);

 /* ll_cat_click = (LinearLayout) itemView.findViewById(R.id.ll_cat_click);
 img_cat = (ImageView) itemView.findViewById(R.id.img_cat);
 */
        }
    }

    public class MyNewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_product, iv_wishlist;
        LinearLayout lv_product_click, lv_add_to_cart, lv_variable_spinner, lv_decrese, lv_increse, lv_add_quantity;
        TextView tv_add_to_cart, tv_save_rs, tv_product_title, tv_old_price, tv_new_price, tv_product_quantity, tv_minus, tv_add;

        public MyNewHolder(View itemView) {
            super(itemView);
            iv_product = (ImageView) itemView.findViewById(R.id.iv_product1);
            iv_wishlist = (ImageView) itemView.findViewById(R.id.iv_wishlist1);

            lv_product_click = (LinearLayout) itemView.findViewById(R.id.lv_product_click1);
            lv_add_to_cart = (LinearLayout) itemView.findViewById(R.id.lv_add_to_cart1);
            lv_decrese = (LinearLayout) itemView.findViewById(R.id.lv_decrese1);
            lv_increse = (LinearLayout) itemView.findViewById(R.id.lv_increse1);
            lv_add_quantity = (LinearLayout) itemView.findViewById(R.id.lv_add_quantity1);
            lv_variable_spinner = (LinearLayout) itemView.findViewById(R.id.lv_variable_spinner1);

            tv_add_to_cart = (TextView) itemView.findViewById(R.id.tv_add_to_cart1);
            tv_add = (TextView) itemView.findViewById(R.id.tv_add1);
            tv_minus = (TextView) itemView.findViewById(R.id.tv_minus1);
            tv_product_quantity = (TextView) itemView.findViewById(R.id.tv_product_quantity1);
            tv_save_rs = (TextView) itemView.findViewById(R.id.tv_save_rs1);
            tv_product_title = (TextView) itemView.findViewById(R.id.tv_product_title1);
            tv_old_price = (TextView) itemView.findViewById(R.id.tv_old_price1);
            tv_new_price = (TextView) itemView.findViewById(R.id.tv_new_price1);

 /* ll_cat_click = (LinearLayout) itemView.findViewById(R.id.ll_cat_click);
 img_cat = (ImageView) itemView.findViewById(R.id.img_cat);
 */
        }
    }
  /*  @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return position % 2 * 2;
    }*/
}





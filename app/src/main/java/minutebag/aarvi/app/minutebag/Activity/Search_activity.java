package minutebag.aarvi.app.minutebag.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import minutebag.aarvi.app.minutebag.Adapter.Explore_product_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Shop_list_adapter;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Retrofit.ApiClient;
import minutebag.aarvi.app.minutebag.Retrofit.ApiInterface;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Search_activity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    SearchView searchview;
    RecyclerView recycler_search_product;
    Explore_product_adapter adapter;
    public static ArrayList<Shop_cat_model> product_model = new ArrayList<Shop_cat_model>();
    WrapContentLinearLayoutManager layoutManager;
    ProgressBar progress_search_product;
    TextView tv_data_not_found_search;
    int page = 1,pageno;
    NestedScrollView nested_scroll_search;


    ///
    //varible for pagination
    public static boolean isLoading = true;
    public static int pastvisibleitem, visibleitemcount, totalitemcount, previous_total = 0;
    public static int view_threshold = 10;
    public static NestedScrollView nestedscroll_shop;

    public static Dialog dialog, dialogcat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setTitle("Search");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AllocateMemory();
        searchview.setOnQueryTextListener(this);
    }

    private void CALL_EXPLORE_PRODUCT_SEARCH_API(final String search_string, int page) {
        progress_search_product.setVisibility(View.GONE);
         // enableProgressBar();

        Log.e("search_string_66", "" + search_string);
        product_model.clear();
        pageno=page;
        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_search_list = api.get_Searchlist_list(String.valueOf(pageno), Main_Url.pass_per_page, search_string);

        get_search_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.e(" res_search", "" + response.body().toString());
                // JSONObject jsonObject = new JSONObject(response.body().string());

                Log.e("api_search_id_172", "" + search_string);
                Log.e("res_shop_175", "" + response);

                product_model.clear();
                recycler_search_product.setVisibility(View.VISIBLE);
                // tvNoDataFound.setVisibility(View.GONE);
               // dialog.dismiss();
                // lv_final_shop_click.setVisibility(View.VISIBLE);
                JSONArray jsonArray = null;
                try {
                    Log.e("jsonArray", "" + jsonArray);

                    jsonArray = new JSONArray(response.body().string());
                    if (jsonArray != null && jsonArray.isNull(0) != true) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject json_object = jsonArray.getJSONObject(i);
                                JSONObject object = null;
                                JSONArray product_img_array = json_object.getJSONArray("images");
                                for (int j = 0; j < product_img_array.length(); j++) {
                                    object = product_img_array.getJSONObject(j);
                                    // Log.e("object_image", "" + object.getString("src"));
                                }
                                product_model.add(new Shop_cat_model(
                                        object.getString("src"),
                                        json_object.getString("name"),
                                        json_object.getString("regular_price"),
                                        json_object.getString("price"),
                                        json_object.getString("name"),
                                        json_object.getString("name"),
                                        json_object.getString("id")));

                            } catch (Exception e) {
                                // Log.e("Exception", "aaaa=" + e);
                            } finally {
                              //  adapter.notifyItemChanged(i);
                            }
                        }
                        adapter = new Explore_product_adapter(Search_activity.this, product_model);
                        layoutManager = new WrapContentLinearLayoutManager(Search_activity.this, 2);
                        recycler_search_product.setLayoutManager(layoutManager);
                        recycler_search_product.setAdapter(adapter);

                        if (product_model.size() == 0) {
                            tv_data_not_found_search.setVisibility(View.VISIBLE);
                        } else {
                            tv_data_not_found_search.setVisibility(View.GONE);
                        }
                    } else {
                        tv_data_not_found_search.setVisibility(View.VISIBLE);
                        recycler_search_product.setVisibility(View.GONE);
                        // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(Search_activity.this, "" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Failure", "" + t.getLocalizedMessage());
            }
        });

        ////////////////////
        nested_scroll_search.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        visibleitemcount = layoutManager.getChildCount();
                        totalitemcount = layoutManager.getItemCount();
                        pastvisibleitem = layoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {

                            if ((visibleitemcount + pastvisibleitem) >= totalitemcount) {
                                pageno++;
                                Log.e("isloading", "loadscroll" + pageno);
                                progress_search_product.setVisibility(View.VISIBLE);
                                PerformPagination(search_string, pageno);
                                //  isLoading=true;
//                        Load Your Data
                            }
                        }
                    }
                }
            }
        });


    }
    public  void enableProgressBar() {
        dialog = new Dialog(Search_activity.this);
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    private void PerformPagination(final String search_string, int pagenoo) {

        //  enableProgressBar();
        // lv_final_shop_click.setVisibility(View.GONE);
        Log.e("search_string_66", "" + search_string);
        pageno=pagenoo;
        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_search_list = api.get_Searchlist_list(String.valueOf(pageno), Main_Url.pass_per_page, search_string);

        get_search_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.e(" res_search", "" + response.body().toString());
                // JSONObject jsonObject = new JSONObject(response.body().string());

                Log.e("api_search_id_172", "" + search_string);
                Log.e("res_shop_175", "" + response);

                product_model.clear();
                progress_search_product.setVisibility(View.GONE);

                recycler_search_product.setVisibility(View.VISIBLE);
                // tvNoDataFound.setVisibility(View.GONE);
                //dialog.dismiss();
                // lv_final_shop_click.setVisibility(View.VISIBLE);
                JSONArray jsonArray = null;
                try {
                    Log.e("jsonArray", "" + jsonArray);

                    jsonArray = new JSONArray(response.body().string());
                    if (jsonArray != null && jsonArray.isNull(0) != true) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject json_object = jsonArray.getJSONObject(i);
                                JSONObject object = null;
                                JSONArray product_img_array = json_object.getJSONArray("images");
                                for (int j = 0; j < product_img_array.length(); j++) {
                                    object = product_img_array.getJSONObject(j);
                                    // Log.e("object_image", "" + object.getString("src"));
                                }
                                product_model.add(new Shop_cat_model(
                                        object.getString("src"),
                                        json_object.getString("name"),
                                        json_object.getString("regular_price"),
                                        json_object.getString("price"),
                                        json_object.getString("name"),
                                        json_object.getString("name"),
                                        json_object.getString("id")));

                            } catch (Exception e) {
                                // Log.e("Exception", "aaaa=" + e);
                            } finally {
                                //  adapter.notifyItemChanged(i);
                            }
                        }
                        adapter = new Explore_product_adapter(Search_activity.this, product_model);
                        layoutManager = new WrapContentLinearLayoutManager(Search_activity.this, 2);
                        recycler_search_product.setLayoutManager(layoutManager);
                        recycler_search_product.setAdapter(adapter);

                        if (product_model.size() == 0) {
                            tv_data_not_found_search.setVisibility(View.VISIBLE);
                        } else {
                            tv_data_not_found_search.setVisibility(View.GONE);
                        }
                    } else {
                        tv_data_not_found_search.setVisibility(View.VISIBLE);
                        recycler_search_product.setVisibility(View.GONE);
                        // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(Search_activity.this, "" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Failure", "" + t.getLocalizedMessage());
            }
        });

    }

    private void AllocateMemory() {
        searchview = (SearchView) findViewById(R.id.searchview);
        recycler_search_product = (RecyclerView) findViewById(R.id.recycler_search_product);
        progress_search_product = (ProgressBar) findViewById(R.id.progress_search_product);
        tv_data_not_found_search = (TextView) findViewById(R.id.tv_data_not_found_search);
        nested_scroll_search = (NestedScrollView) findViewById(R.id.nested_scroll_search);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cart, menu);
/*
        Log.e("countsubjava", "" + count1);
        MenuItem menuItem = menu.findItem(R.id.wish);
        icon = (LayerDrawable) menuItem.getIcon();

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(Sub_category.this);
        }
        // Log.e("countt",""+count);
        if (count1 == "" || count1 == null || count1 == "null" || count1.equalsIgnoreCase(null)
                || count1.equalsIgnoreCase("null")) {
            count1 = String.valueOf(0);
            Log.e("countt", "" + count1);
        }
        badge.setCount(count1);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
/*
            Log.e("checkscreen_else", "" + CheckScreen);
            onBackPressed();*/
        } else if (item.getItemId() == R.id.cart) {

            startActivity(new Intent(Search_activity.this, Cart_Activity.class));
        }   // Toast.makeText(getApplicationContext(), "wishlist", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String search_string = s;
        Log.e("search_string", "" + search_string);

        if (CheckNetwork.isNetworkAvailable(Search_activity.this)) {
            product_model.clear();
            CALL_EXPLORE_PRODUCT_SEARCH_API(search_string, page);
        } else {
            Toast.makeText(Search_activity.this, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}


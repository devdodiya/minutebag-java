package minutebag.aarvi.app.minutebag.Fragment;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.List;

import minutebag.aarvi.app.minutebag.Model.Slider_cat_model;
import minutebag.aarvi.app.minutebag.Model.sliderimage_model;
import minutebag.aarvi.app.minutebag.R;


    class Sliding_Category_image_adapter extends PagerAdapter {
        Context context;
        private List<Slider_cat_model> sliderimage_models;
        private LayoutInflater inflater;
        String screen;

        public Sliding_Category_image_adapter(Context context, List<Slider_cat_model> sliderimage_models) {

            this.context = context;
            this.sliderimage_models = sliderimage_models;
            inflater = LayoutInflater.from(context);

            }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }
        @Override
        public int getCount() {
            return sliderimage_models.size();
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final ImageView imageview_category;

            Log.e("screennnnnn",""+screen);
            View view = null;

            inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.home_slide_row,container,false);

            final Slider_cat_model sm = sliderimage_models.get(position);
            imageview_category = (ImageView)view.findViewById(R.id.iv_home_main_banner);

           // imageview_category = (ImageView)view.findViewById(R.id.imageview_category);
            Glide.with(context).load(sm.getBanners_image()).into(imageview_category);
            Log.e("image_banner",""+sm.getBanners_image());

            //Glide.with(context).load(sm.getCategory_image_latest()).into(imageview_category);


            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==(LinearLayout)object);
        }
    }


package minutebag.aarvi.app.minutebag.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class About_Us extends AppCompatActivity
{
    LinearLayout txtwebsitea,txtprivacya,txttermsa,txtrefunda;
    Context ctx=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        setTitle("About Us");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        allocatememory();
        setevent();
        }

    private void setevent()
    {
        txtwebsitea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(ctx, SettingView.class);
                intent.putExtra("set_id",Main_Url.WEBSITE_ID);
                startActivity(intent);
            }
        });
        txtprivacya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(ctx, SettingView.class);
                intent.putExtra("set_id", Main_Url.POLICY_ID);
                startActivity(intent);
            }
        });
        txttermsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(ctx, SettingView.class);
                intent.putExtra("set_id",Main_Url.TERMS_ID);
                startActivity(intent);
            }
        });
        txtrefunda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(ctx, SettingView.class);
                intent.putExtra("set_id",Main_Url.REFUND_ID);
                startActivity(intent);
            }
        });
    }

    private void allocatememory()
    {
        txtwebsitea=findViewById(R.id.txtwebsitea);
        txtprivacya=findViewById(R.id.txtprivacya);
        txttermsa=findViewById(R.id.txttermsa);
        txtrefunda=findViewById(R.id.txtrefunda);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart, menu);
/*
        Log.e("countsubjava", "" + count1);
        MenuItem menuItem = menu.findItem(R.id.wish);
        icon = (LayerDrawable) menuItem.getIcon();

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(Sub_category.this);
        }
        // Log.e("countt",""+count);
        if (count1 == "" || count1 == null || count1 == "null" || count1.equalsIgnoreCase(null)
                || count1.equalsIgnoreCase("null")) {
            count1 = String.valueOf(0);
            Log.e("countt", "" + count1);
        }
        badge.setCount(count1);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
/*
            Log.e("checkscreen_else", "" + CheckScreen);
            onBackPressed();*/
        }  else if (item.getItemId() == R.id.cart_item) {

            startActivity(new Intent(About_Us.this, Cart_Activity.class));
        }
        else if (item.getItemId() == R.id.search) {

            startActivity(new Intent(About_Us.this, Search_activity.class));
        }// Toast.makeText(getApplicationContext(), "wishlist", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

}

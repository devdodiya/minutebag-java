package minutebag.aarvi.app.minutebag.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import minutebag.aarvi.app.minutebag.Adapter.Product_detail_img_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Related_item_adapter;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.Model.sliderimage_model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class Product_Detail extends AppCompatActivity {
    private SlidingImage_Adapter adapter;
    private ViewPager viewPager;
    private List<sliderimage_model> sliderimage_models = new ArrayList<sliderimage_model>();
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500, PERIOD_MS = 3000;
    private static String URL = Main_Url.sortUrl + "getbannerlist.php?";
    LinearLayout lv_add_to_cart_detail;

    String productid, URL_PRODUCT_DETAIL;
    LinearLayout lv_pdetail_descrese, lv_pdetail_increse, lv_product_desc;
    RatingBar ratingbar_pdetail;
    TextView tv_pdetail_old_price, tv_pdetail_new_price, tv_pdetail_instock, tv_pdetail_product_name,
            tv_pdetail_productshort_desc, tv_rb_pdetail_review, tv_pdetail_quantity, tv_pdetail_quanty,
            tv_desc_allselect, tv_pdescription, tv_description,tv_total_pdetail_price,tv_total_price;
    int product_quantity = 0;
    ArrayList<String> array_item = new ArrayList<String>();

    RecyclerView related_items_recycler;
    public static Related_item_adapter related_item_adapter;
    public static ArrayList<Shop_cat_model> product_model = new ArrayList<Shop_cat_model>();

    String str="";
    public  static Dialog dialog;
    ImageView iv_product_detail;
    CircleIndicator slider_indicator_pdetail;
    IndefinitePagerIndicator recv_indicator;
    //
    Product_detail_img_adapter img_adapter;
    RecyclerView recv_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        setTitle("Product Details");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Allocatememory();

        productid = getIntent().getStringExtra("product_id");

        lv_add_to_cart_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Product_Detail.this, Cart_Activity.class));
            }
        });
        ViewPagerdata();

        if (CheckNetwork.isNetworkAvailable(Product_Detail.this)) {
            CALL_PRODUCT_DETAIL_API();

        } else {
            Toast.makeText(Product_Detail.this, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        lv_pdetail_increse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_quantity = product_quantity + 1;
                Log.e("product_104", "" + product_quantity);
                String result = String.valueOf(product_quantity);
                tv_pdetail_quanty.setText(result);
            }
        });
        lv_pdetail_descrese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product_quantity != 0) {
                    product_quantity -= 1;
                    String result = String.valueOf(product_quantity);
                    Log.e("product_1011_minus", "" + product_quantity);
                    tv_pdetail_quanty.setText(result);
                } else {
                    tv_pdetail_quanty.setText(String.valueOf(product_quantity));
                }
            }
        });

    }

    private void CALL_RELATED_ITEM_API(String str_array) {
        //URLEncoder.encode("[" + bookData.get(position).getBookId() + "]", "UTF-8")
        Log.e("str_array",""+str_array);

            final String URL;
            URL = Main_Url.RELATED_PRODUCT_ITEM_API+ str_array;

        //Log.e("URL_RELATEDITEM",""+URL);
        product_model.clear();
        enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("URL_related", "" + URL);
                        Log.e("response_explore_home", "" + response);
                        try {
                                dialog.dismiss();
                            // flag_ex_product = true;
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("jsonArray_related_api", "" + jsonArray);
                            // progressdialog.dismiss();
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + json_object);
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            Log.e("object_image", "" + object.getString("src"));

                                        }
                                        product_model.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        Log.e("Exception", "aaaa" + e);
                                    } finally {
                                        related_item_adapter.notifyItemChanged(i);
                                    }
                                }
                            } else {
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(Product_Detail.this);
        requestQueue.add(stringRequest);

    }

    private void Allocatememory() {
        recv_indicator=(IndefinitePagerIndicator) findViewById(R.id.recyclerview_pager_indicator);
        slider_indicator_pdetail=(CircleIndicator)findViewById(R.id.slider_indicator_pdetail);
        iv_product_detail=(ImageView) findViewById(R.id.iv_product_detail);
        //related item
        related_items_recycler = (RecyclerView) findViewById(R.id.related_items_recycler);
        recv_img = (RecyclerView) findViewById(R.id.recv_img);
        related_item_adapter = new Related_item_adapter(Product_Detail.this, product_model);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Product_Detail.this, LinearLayoutManager.HORIZONTAL, false);
        related_items_recycler.setLayoutManager(layoutManager);
        related_items_recycler.setAdapter(related_item_adapter);

        lv_add_to_cart_detail = (LinearLayout) findViewById(R.id.lv_add_to_cart_detail);
        lv_pdetail_descrese = (LinearLayout) findViewById(R.id.lv_pdetail_descrese);
        lv_pdetail_increse = (LinearLayout) findViewById(R.id.lv_pdetail_increse);
        lv_product_desc = (LinearLayout) findViewById(R.id.lv_product_desc);
        tv_total_pdetail_price = (TextView) findViewById(R.id.tv_total_pdetail_price);
        tv_total_price = (TextView) findViewById(R.id.tv_total_price);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_pdetail_old_price = (TextView) findViewById(R.id.tv_pdetail_old_price);
        tv_pdetail_new_price = (TextView) findViewById(R.id.tv_pdetail_new_price);
        tv_pdetail_instock = (TextView) findViewById(R.id.tv_pdetail_instock);
        tv_pdetail_product_name = (TextView) findViewById(R.id.tv_pdetail_product_name);
        tv_pdetail_productshort_desc = (TextView) findViewById(R.id.tv_pdetail_productshort_desc);
        tv_rb_pdetail_review = (TextView) findViewById(R.id.tv_rb_pdetail_review);
        tv_pdetail_quantity = (TextView) findViewById(R.id.tv_pdetail_quantity);
        tv_pdetail_quanty = (TextView) findViewById(R.id.tv_pdetail_quanty);
        tv_desc_allselect = (TextView) findViewById(R.id.tv_desc_allselect);
        tv_pdescription = (TextView) findViewById(R.id.tv_pdescription);
        ratingbar_pdetail = (RatingBar) findViewById(R.id.ratingbar_pdetail);


        //////
        img_adapter = new Product_detail_img_adapter(Product_Detail.this, sliderimage_models);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(Product_Detail.this, LinearLayoutManager.HORIZONTAL, false);
        recv_img.setLayoutManager(layoutManager1);
        recv_img.setAdapter(img_adapter);
        recv_indicator.attachToRecyclerView(recv_img);


    }
    public void enableProgressBar() {
        dialog = new Dialog(Product_Detail.this);
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar=(ProgressBar)dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    private void CALL_PRODUCT_DETAIL_API() {
        sliderimage_models.clear();
        URL_PRODUCT_DETAIL = Main_Url.sortUrl + "products/" + productid + "?consumer_key=" + Main_Url.consumer_key +
                "&consumer_secret=" + Main_Url.consumer_secret;
        Log.e("product_detail_url", "" + URL_PRODUCT_DETAIL);
        Log.e("productid", "" + productid);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCT_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("product_detail_url", "" + URL_PRODUCT_DETAIL);
                        try {
                            str="";
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("response_prod_detail", "" + jsonObject);
                            String slider = jsonObject.getString("images");
                            Log.e("slider", "" + slider);

                            JSONArray images_arr = new JSONArray(slider);
                            Log.e("arr_266", "" + images_arr);
                            for (int i = 0; i < images_arr.length(); i++) {
                                try {
                                    JSONObject object = images_arr.getJSONObject(i);
                                    Log.e("object", "" + object);
                                    Log.e("title", "" + object.getString("id"));
                                    Log.e("url_272", "" + object.getString("src"));
                                    //iv_product_detail.setVisibility(View.VISIBLE);
                                    viewPager.setVisibility(View.GONE);
                                    slider_indicator_pdetail.setVisibility(View.GONE);
                                   // Glide.with(Product_Detail.this).load(object.getString("src")).into(iv_product_detail);
                                    sliderimage_model imageModel = new sliderimage_model(object.getString("src"),
                                            object.getString("name"), object.getString("id"));
                                    sliderimage_models.add(imageModel);
                                    Log.e("schedule", "" + sliderimage_models.size());
                                } catch (Exception e) {
                                    Log.e("Exception", "" + e);
                                } finally {
                                    img_adapter.notifyDataSetChanged();
                                }
                            }

                           // tv_pdetail_product_name.setText(jsonObject.getString("name"));
                           // tv_description.setText(Html.fromHtml(jsonObject.getString("description")));
                           // tv_pdetail_productshort_desc.setText(jsonObject.getString("short_description"));
                           // tv_pdetail_instock.setText("In Stock" + "(" + jsonObject.getString("stock_quantity") + ")");
                           // tv_pdetail_old_price.setText(getResources().getString(R.string.rs) + jsonObject.getString("regular_price"));
                           // tv_pdetail_new_price.setText(getResources().getString(R.string.rs) + jsonObject.getString("sale_price"));
                           // tv_total_pdetail_price.setText("RS."+ jsonObject.getString("sale_price"));


                            Navigation_activity.Check_String_NULL_Value(tv_pdetail_product_name,jsonObject.getString("name"));
                            Navigation_activity.Check_String_NULL_Value(tv_description,jsonObject.getString("description"));
                            Navigation_activity.Check_String_NULL_Value(tv_pdetail_productshort_desc,jsonObject.getString("short_description"));
                            Navigation_activity.Check_String_NULL_Value(tv_pdetail_instock,"In Stock" + "(" + jsonObject.getString("stock_quantity") + ")");
                            Navigation_activity.Check_String_NULL_Value(tv_pdetail_old_price,getResources().getString(R.string.rs) + jsonObject.getString("regular_price"));
                            Navigation_activity.Check_String_NULL_Value(tv_pdetail_new_price,getResources().getString(R.string.rs) + jsonObject.getString("sale_price"));
                            Navigation_activity.Check_String_NULL_Value(tv_total_pdetail_price,"RS."+ jsonObject.getString("sale_price"));

                            tv_pdetail_old_price.setPaintFlags(tv_pdetail_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            JSONArray related_item_array=jsonObject.getJSONArray("related_ids");
                            for(int j=0;j<related_item_array.length();j++)
                            {
                             str=str + "&include[]=" + related_item_array.getInt(j);

                            }
                            Log.e("array_item",""+str);
                            CALL_RELATED_ITEM_API(str);


                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Toast.makeText(getActivity()), "not get Response", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(Product_Detail.this);
        requestQueue.add(stringRequest);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void ViewPagerdata() {
        viewPager = (ViewPager) findViewById(R.id.imgpager_product_detail);

        adapter = new SlidingImage_Adapter(Product_Detail.this, sliderimage_models);
        viewPager.setAdapter(adapter);
        slider_indicator_pdetail.setViewPager(viewPager);
        Log.e("schedule", "" + sliderimage_models.size());

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == sliderimage_models.size() - 1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);



    }

}

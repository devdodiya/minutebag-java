package minutebag.aarvi.app.minutebag.Adapter;


import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import minutebag.aarvi.app.minutebag.Activity.Final_button_shop_activity;
import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Model.AddToCart;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;


public class Cart_Adapter extends RecyclerView.Adapter<Cart_Adapter.MyViewHolder> {
    // private List<AddToCart> category_models;
    public static List<AddToCart> subcat_model;
    public int mSelectedItem = -1;

    private Context context;
    LayoutInflater inflater;
    public static String cat_id;
    int selectedPosition;
    public static int page_no = 1;

    public Cart_Adapter(Context context, List<AddToCart> product_model) {
        this.context = context;
        this.subcat_model = product_model;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedPosition = 0;
    }

    @Override
    public Cart_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.cart_row, parent, false);
        return new Cart_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Cart_Adapter.MyViewHolder holder, final int position) {

        final AddToCart model = subcat_model.get(position);
        // String title = model.getCategory_title();
        // holder.tv_cat_title.setText(Html.fromHtml(title));
        Navigation_activity.Check_String_NULL_Value(holder.tv_cart_title, model.getProductName());
       // Navigation_activity.Check_String_NULL_Value(holder.tv_cart_old_price, model.getOldPrice());
        //Navigation_activity.Check_String_NULL_Value(holder.tv_cart_new_price, model.getNewPrice());
        Navigation_activity.Check_String_NULL_Value(holder.tv_cart_old_price, context.getResources().getString(R.string.rs) + model.getOldPrice());
        Navigation_activity.Check_String_NULL_Value(holder.tv_cart_new_price, context.getResources().getString(R.string.rs) + model.getNewPrice());
        holder.tv_cart_old_price.setPaintFlags(holder.tv_cart_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Navigation_activity.Check_String_NULL_Value(holder.tv_cart_sub_total, String.valueOf(model.getTotal()));
        Navigation_activity.Check_String_NULL_Value(holder.tv_cart_quantity_total, String.valueOf(model.getIncrease()));

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.drawable_ldpi_icon);
        requestOptions.error(R.drawable.drawable_ldpi_icon);
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(model.getImage()).into(holder.iv_cart_img);


        holder.lv_cart_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   Toast.makeText(context, "click"+subcat_model.get(position).getCategory_title(), Toast.LENGTH_SHORT).show();
              /*  Final_button_shop_activity.CALL_RETROFIT_SHOP_API(subcat_model.get(position).getCategory_id(), page_no, Main_Url.GRID_TYPE);
                mSelectedItem = position;
                notifyDataSetChanged();*/

            }
        });

    }


    @Override
    public int getItemCount() {
        Log.e("modelsize", "" + subcat_model.size());
        return subcat_model.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cart_sub_total, tv_cart_quantity_total,
                tv_cart_title, tv_cart_new_price, tv_cart_old_price;
        public ImageView iv_cart_img;
        LinearLayout lv_cart_remove, lv_cart_view, lv_cart_increse, lv_cart_decrese;
        // View view_shop;

        public MyViewHolder(View itemView) {
            super(itemView);

            // view_shop = (View) itemView.findViewById(R.id.view_shop);
            tv_cart_sub_total = (TextView) itemView.findViewById(R.id.tv_cart_sub_total);
            tv_cart_quantity_total = (TextView) itemView.findViewById(R.id.tv_cart_quantity_total);
            tv_cart_title = (TextView) itemView.findViewById(R.id.tv_cart_title);
            tv_cart_new_price = (TextView) itemView.findViewById(R.id.tv_cart_new_price);
            tv_cart_old_price = (TextView) itemView.findViewById(R.id.tv_cart_old_price);
            iv_cart_img = (ImageView) itemView.findViewById(R.id.iv_cart_img);
            lv_cart_remove = (LinearLayout) itemView.findViewById(R.id.lv_cart_remove);
            lv_cart_view = (LinearLayout) itemView.findViewById(R.id.lv_cart_view);
            lv_cart_increse = (LinearLayout) itemView.findViewById(R.id.lv_cart_increse);
            lv_cart_decrese = (LinearLayout) itemView.findViewById(R.id.lv_cart_decrese);
            // iv_image = (ImageView) itemView.findViewById(R.id.iv_image);
        }

    }


}






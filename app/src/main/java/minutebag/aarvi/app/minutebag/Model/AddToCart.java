package minutebag.aarvi.app.minutebag.Model;



public class AddToCart {
    private String oldPrice,newPrice,productName,image;
    private int Increase;
    String total,id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AddToCart(String oldPrice, String newPrice, String productName, String image, int increase, String id, String total) {
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
        this.productName = productName;
        this.image = image;
        Increase = increase;
        this.id = id;
        this.total = total;
    }


    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getIncrease() {
        return Increase;
    }

    public void setIncrease(int increase) {
        Increase = increase;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}

package minutebag.aarvi.app.minutebag.Retrofit;

import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    public  String consumer_key = "ck_0785376043847cba9836a37ccde4002bec4fb5f0";
    public  String consumer_secret = "cs_e037ae395709ad316322afe573fec7383b05df5f";

    //////explore product home
///https://minutebag.com/wp-json/wc/v3/products?
// consumer_key=ck_0785376043847cba9836a37ccde4002bec4fb5f0&consumer_secret
// =cs_e037ae395709ad316322afe573fec7383b05df5f&page=1&per_page=10&category=17

    public String EXPLORE_HOME_PRODUCT =  "products?consumer_key=" + consumer_key +
            "&consumer_secret=" + consumer_secret;

    @GET(EXPLORE_HOME_PRODUCT)
    Call<ResponseBody> get_Shop_list(@Query("page") String page,
                                     @Query("per_page") String per_page,
                                     @Query("category") String category
                                    );


    ////search api
    @GET(EXPLORE_HOME_PRODUCT)
    Call<ResponseBody> get_Searchlist_list(@Query("page") String page,
                                     @Query("per_page") String per_page,
                                     @Query("search") String search);

    //sort api a to z

   // order=asc,order=desc
    @GET(EXPLORE_HOME_PRODUCT)
    Call<ResponseBody> get_Sort_list(@Query("page") String page,
                                           @Query("per_page") String per_page,
                                            @Query("category") String category,
                                            @Query("order") String order);


}

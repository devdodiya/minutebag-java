package minutebag.aarvi.app.minutebag.Fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


import minutebag.aarvi.app.minutebag.Activity.New_Shop_activity;
import minutebag.aarvi.app.minutebag.Activity.WrapContentLinearLayoutManager;
import minutebag.aarvi.app.minutebag.Adapter.Explore_product_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Shop_list_adapter;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Retrofit.ApiClient;
import minutebag.aarvi.app.minutebag.Retrofit.ApiInterface;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;


public class New_dynamic_Library_frag extends Fragment {

    private static final String ARG_CATEGORY_ID = "category_id";
    private static final String ARG_CATEGORY_TITLE = "category_title";
    private static final String ARG_VISIBLE = "visible";
    public static List<Shop_cat_model> itemArrayList = new ArrayList<Shop_cat_model>();
    private RecyclerView recyclerviewSubCategory;

    private Shop_list_adapter adapter;
    private String category_id, category_title;
    private ProgressBar progressBar_library;
    private TextView tvNoDataFound;

    private boolean isVisible;
    public static List<String> cat_id_list = new ArrayList<String>();
    TextView textView;
    public static WrapContentLinearLayoutManager layoutManager;

    public static int page_no = 1, per_page = 10, product_pageno;
    public static ProgressBar progress_shop_tab;
    public static NestedScrollView nestedscroll_shop_tab;
    //varible for pagination

    public static int item_count = 15, page_val;
    public static boolean isLoading = true;
    public static int pastvisibleitem, visibleitemcount, totalitemcount, previous_total = 0;
    public static int view_threshold = 10;
    public static int new_val;
    public static Dialog dialog, dialog_explore;

    public static New_dynamic_Library_frag newInstance(String sectionNumber, boolean visible, String category_title) {
        New_dynamic_Library_frag fragment = new New_dynamic_Library_frag();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY_ID, sectionNumber);
        args.putBoolean(ARG_VISIBLE, visible);
        args.putString(ARG_CATEGORY_TITLE, category_title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new_dynamic_library_frag, container, false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cat_id_list.clear();

        ALLOCATEMEMORY(view);

        if (getArguments() != null) {
            category_id = getArguments().getString(ARG_CATEGORY_ID, "");
            isVisible = getArguments().getBoolean(ARG_VISIBLE, false);
            category_title = getArguments().getString(ARG_CATEGORY_TITLE, "");
        }

        cat_id_list.add(category_id);
        Log.e("cat_id_106", "" + category_id);
        Log.e("cat_id_106size", "" + cat_id_list.size());
        textView.setText(category_id + " =" + category_title);
        // Log.e("Tab_name_115", "" + category_title);
        //Log.e("Tab_visible_115", "" + isVisible);

        for (int i = 0; i < cat_id_list.size(); i++) {
            itemArrayList.clear();

            Log.e("get_pass_id_115", "" + cat_id_list.get(i));
            if (CheckNetwork.isNetworkAvailable(getActivity())) {
                itemArrayList.clear();
                CALL_SHOP_PRODUCT_API(cat_id_list.get(i), page_no);

                //CALL_RETROFIT_SHOP_API(cat_id_list.get(i), page_no);


            } else {
                Toast.makeText(getActivity(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }


        }
        cat_id_list.clear();


    }

    private void CALL_RETROFIT_SHOP_API(final String category_id1, int page_no) {

        Log.e("category_id_108", "" + category_id1);
        itemArrayList.clear();
        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_library_list = api.get_Shop_list(String.valueOf(page_no), Main_Url.per_page, category_id);
        get_library_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.e(" shop_retro_res", "" + response.body().toString());
                // JSONObject jsonObject = new JSONObject(response.body().string());

                Log.e("api_cat_id_172", "" + category_id1);
                Log.e("res_shop_175", "" + response);

                itemArrayList.clear();
                recyclerviewSubCategory.setVisibility(View.VISIBLE);
                tvNoDataFound.setVisibility(View.GONE);
                //  dialog.dismiss();
                JSONArray jsonArray = null;
                try {
                    Log.e("jsonArray", "" + jsonArray);

                    jsonArray = new JSONArray(response.body().string());
                    if (jsonArray != null && jsonArray.isNull(0) != true) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject json_object = jsonArray.getJSONObject(i);
                                JSONObject object = null;
                                JSONArray product_img_array = json_object.getJSONArray("images");
                                for (int j = 0; j < product_img_array.length(); j++) {
                                    object = product_img_array.getJSONObject(j);
                                    // Log.e("object_image", "" + object.getString("src"));
                                }
                                itemArrayList.add(new Shop_cat_model(
                                        object.getString("src"),
                                        json_object.getString("name"),
                                        json_object.getString("regular_price"),
                                        json_object.getString("price"),
                                        json_object.getString("name"),
                                        json_object.getString("name"),
                                        json_object.getString("id")));

                            } catch (Exception e) {
                                // Log.e("Exception", "aaaa=" + e);
                            } finally {
                                //    adapter.notifyItemChanged(i);
                            }
                        }
                        adapter = new Shop_list_adapter(getActivity(), itemArrayList);
                        layoutManager = new WrapContentLinearLayoutManager(getActivity(), 2);
                        // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                        recyclerviewSubCategory.setLayoutManager(layoutManager);
                        recyclerviewSubCategory.setAdapter(adapter);

                        if (itemArrayList.size() == 0) {
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        } else {
                            tvNoDataFound.setVisibility(View.GONE);
                        }
                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE);
                        recyclerviewSubCategory.setVisibility(View.GONE);
                        tvNoDataFound.setVisibility(View.VISIBLE);
                        // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Failure", "" + t.getLocalizedMessage());
            }
        });


    }

    public void enableProgressBar() {
        dialog = new Dialog(getActivity());
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    public void CALL_SHOP_PRODUCT_API(final String catid, int page_no) {
        itemArrayList.clear();
        product_pageno = page_no;
        final String URL;
        URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                + product_pageno + "&per_page=" + per_page + "&category=" + catid;
        //&page=1&per_page=10&category=17
       /* if (catid.equals(0) == true || catid.equals("0") == true ) {

            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_pageno + "&per_page=" + per_page;
            Log.e("URL_all_157", ""+URL);

        } else {
            Log.e("catid_148", "" + catid);
            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_pageno + "&per_page=" + per_page + "&category=" + catid;
            Log.e("URL_category", ""+URL);
        }*/

        //  progress_shop_tab.setVisibility(View.VISIBLE);
        // enableProgressBar();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //.setVisibility(View.GONE);

                        Log.e("api_cat_id_172", "" + catid);
                        Log.e("URL_shop_product", "" + URL);
                        Log.e("res_shop_175", "" + response);
                        try {
                            itemArrayList.clear();
                            recyclerviewSubCategory.setVisibility(View.VISIBLE);
                            tvNoDataFound.setVisibility(View.GONE);
                            //  dialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            // Log.e("object_image", "" + object.getString("src"));

                                        }
                                        itemArrayList.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        // Log.e("Exception", "aaaa=" + e);
                                    } finally {
                                        //    adapter.notifyItemChanged(i);
                                    }
                                }
                                adapter = new Shop_list_adapter(getActivity(), itemArrayList);
                                layoutManager = new WrapContentLinearLayoutManager(getActivity(), 2);
                                // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                                recyclerviewSubCategory.setLayoutManager(layoutManager);
                                recyclerviewSubCategory.setAdapter(adapter);

                                if (itemArrayList.size() == 0) {
                                    tvNoDataFound.setVisibility(View.VISIBLE);
                                } else {
                                    tvNoDataFound.setVisibility(View.GONE);
                                }
                            } else {
                                tvNoDataFound.setVisibility(View.VISIBLE);
                                recyclerviewSubCategory.setVisibility(View.GONE);
                                tvNoDataFound.setVisibility(View.VISIBLE);
                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }

                            progress_shop_tab.setVisibility(View.GONE);
                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                                Log.e("erroraaa", "aaaa" + obj);
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                e2.printStackTrace();
                            }
                        }
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

/*
        /////////////////////
        nestedscroll_shop_tab.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        visibleitemcount = layoutManager.getChildCount();
                        totalitemcount = layoutManager.getItemCount();
                        pastvisibleitem = layoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {

                            if ((visibleitemcount + pastvisibleitem) >= totalitemcount) {
                                product_pageno++;
                                Log.e("isloading", "loadscroll" + product_pageno);

                                Perform_Pagination(catid,product_pageno);
                                //  isLoading=true;
//                        Load Your Data
                            }
                        }
                    }
                }
            }
        });

*/

    }

    public void Perform_Pagination(final String catid, int page) {
        product_pageno = page;

        final String URL;
        //&page=1&per_page=10&category=17

        if (catid.equals("0") == true || catid.equals("0") == true) {
            Log.e("catid_pagi", "catid" + catid);
            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_pageno + "&per_page=" + per_page;
        } else {
            Log.e("catid_250_pagi", "" + catid);
            URL = Main_Url.EXPLORE_HOME_PRODUCT + "&page="
                    + product_pageno + "&per_page=" + per_page + "&category=" + catid;
        }
        // String cat_id = Login_preference.getcatid(context);
        progress_shop_tab.setVisibility(View.VISIBLE);
        itemArrayList.clear();
        // enableProgressBar();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("subcatid", "" + catid);
                        Log.e("URL_shop_pagi", "" + URL);
                        Log.e("response_shop_pagi_293", "" + response);
                        try {
                            // flag_ex_product = true;
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("jsonArray_shop", "" + jsonArray);
                            //  dialog.dismiss();
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject json_object = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + json_object);
                                        Log.e("name", "" + json_object.getString("name"));
                                        Log.e("regular_price", "" + json_object.getString("regular_price"));
                                        Log.e("price", "" + json_object.getString("price"));
                                        Log.e("id", "" + json_object.getString("id"));
                                        JSONObject object = null;
                                        JSONArray product_img_array = json_object.getJSONArray("images");
                                        for (int j = 0; j < product_img_array.length(); j++) {
                                            object = product_img_array.getJSONObject(j);
                                            Log.e("object_image", "" + object.getString("src"));

                                        }
                                        itemArrayList.add(new Shop_cat_model(
                                                object.getString("src"),
                                                json_object.getString("name"),
                                                json_object.getString("regular_price"),
                                                json_object.getString("price"),
                                                json_object.getString("name"),
                                                json_object.getString("name"),
                                                json_object.getString("id")));

                                    } catch (Exception e) {
                                        Log.e("Exception", "aaaa=" + e);
                                    } finally {
                                        adapter.notifyItemChanged(i);
                                    }
                                }
                                adapter = new Shop_list_adapter(getActivity(), itemArrayList);
                                recyclerviewSubCategory.setAdapter(adapter);
                                recyclerviewSubCategory.setLayoutManager(new LinearLayoutManager(getActivity()));

                                if (itemArrayList.size() == 0) {
                                    tvNoDataFound.setVisibility(View.VISIBLE);
                                } else {
                                    tvNoDataFound.setVisibility(View.GONE);
                                }

                            } else {
                                tvNoDataFound.setVisibility(View.VISIBLE);

                                // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                            }

                            progress_shop_tab.setVisibility(View.GONE);
                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }


    private void ALLOCATEMEMORY(View view) {
        textView = (TextView) view.findViewById(R.id.textView);
        //recycler view
        recyclerviewSubCategory = view.findViewById(R.id.recycler_library);
        //progress bar
        progress_shop_tab = view.findViewById(R.id.progress_shop);
        //text view
        tvNoDataFound = view.findViewById(R.id.tv_data_not_found_library);

    }
/*
    private void CALL_LIBRARY_LIST_API(String category_id1) {
        Log.e("category_id_108", "" + category_id1);
        itemArrayList.clear();
        progressBar_library.setVisibility(View.VISIBLE);

        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_library_list = api.get_library_list(ApiClient.PAGE, ApiClient.PER_PAGE, category_id1, ApiClient.user_status);
        get_library_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e(" ", "" + response.body().toString());
                progressBar_library.setVisibility(View.GONE);

                try {
                    if (response.isSuccessful()) {
                        itemArrayList.clear();
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("msg");
                        recyclerviewSubCategory.setVisibility(View.VISIBLE);
                        tvNoDataFound.setVisibility(View.GONE);

                        Log.e("status_news_detail", "" + status);
                        if (status.equalsIgnoreCase("success")) {
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String library_id = object.getString("library_id");
                                Log.e("library_id", "" + library_id);
                                String library_title = object.getString("library_title");
                                String library_link = object.getString("library_link");
                                String category_id = object.getString("category_id");
                                String library_status = object.getString("library_status");

                                String date = object.getString("library_created_at_format_day") + " " +
                                        object.getString("library_created_at_format_month") + " " +
                                        object.getString("library_created_at_format_year");

                                Library_model model = new Library_model(library_id,
                                        library_title,
                                        library_link,
                                        category_id,
                                        date,
                                        library_status);

                                itemArrayList.add(model);
                            }
                            adapter = new Library_adapter(getActivity(), itemArrayList);
                            recyclerviewSubCategory.setAdapter(adapter);
                            recyclerviewSubCategory.setLayoutManager(new LinearLayoutManager(getActivity()));

                            if (itemArrayList.size() == 0) {
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            } else {
                                tvNoDataFound.setVisibility(View.GONE);
                            }

                        } else {

                            recyclerviewSubCategory.setVisibility(View.GONE);
                            tvNoDataFound.setText(message);
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar_library.setVisibility(View.GONE);
                    Log.e("exception", "" + e.getLocalizedMessage());
                    Toast.makeText(getActivity(), "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    //  Utils.showErrorSnackBar(getView().getRootView(), e.getLocalizedMessage(), Snackbar.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                progressBar_library.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Failure", "" + t.getLocalizedMessage());
            }
        });


    }*/

/*
    public  void Call_Library_API(String cat_id) {

        Log.e("category_id_108", "" + cat_id);

        itemArrayList.clear();
        progressBar_library.setVisibility(View.VISIBLE);

        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_library_list = api.get_library_list(ApiClient.PAGE,ApiClient.PER_PAGE,cat_id,ApiClient.user_status);


        get_library_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("response", "" + response.body().toString());
                progressBar_library.setVisibility(View.GONE);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    Log.e("status_news_detail",""+status);
                    String message=jsonObject.getString("msg");
                    if (status.equalsIgnoreCase("success")){

                        JSONArray data_array=jsonObject.getJSONArray("data");
                        Log.e("data_array",""+data_array);
                        recyclerviewSubCategory.setVisibility(View.VISIBLE);
                        tvNoDataFound.setVisibility(View.GONE);
                        for (int i = 0; i < data_array.length(); i++) {

                            try {
                                JSONObject data_object = data_array.getJSONObject(i);

                                String date=data_object.getString("library_created_at_format_day")+" "+
                                        data_object.getString("library_created_at_format_month")+" "+
                                        data_object.getString("library_created_at_format_year");

                                Log.e("library_id",""+data_object.getString("library_id"));

                                itemArrayList.add(new Library_model(data_object.getString("library_id")
                                        ,data_object.getString("library_title"),
                                        data_object.getString("library_link"),
                                        data_object.getString("category_id"),
                                        date,
                                        data_object.getString("library_status")));

                            } catch (Exception e) {
                                Log.e("Exception", "" + e);
                            } finally {
                                adapter.notifyItemChanged(i);
                            }

                        }
                        if (itemArrayList.size() == 0) {
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        } else {
                            tvNoDataFound.setVisibility(View.GONE);
                        }

                    }else if (status.equalsIgnoreCase("error")){
                        recyclerviewSubCategory.setVisibility(View.GONE);
                        tvNoDataFound.setText(message);
                        tvNoDataFound.setVisibility(View.VISIBLE);
                    }

                }catch (Exception e){
                    Log.e("",""+e);
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });

    }
*/


}

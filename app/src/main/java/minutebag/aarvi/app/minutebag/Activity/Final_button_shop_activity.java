package minutebag.aarvi.app.minutebag.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import minutebag.aarvi.app.minutebag.Adapter.Category_grid_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Shop_list_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Shop_subCategory_Adapter;
import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Model.Cat_Explore_Model;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Retrofit.ApiClient;
import minutebag.aarvi.app.minutebag.Retrofit.ApiInterface;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Final_button_shop_activity extends AppCompatActivity implements View.OnClickListener {
    Shop_subCategory_Adapter cat_adapter;
    public static RecyclerView recv_category, recv_category_shop_data;
    public static Shop_list_adapter adapter;
    public static ArrayList<Category_List_Model> category_list_models_tab = new ArrayList<Category_List_Model>();

    public static List<Shop_cat_model> itemArrayList = new ArrayList<Shop_cat_model>();

    public static Context context = null;
    public static Dialog dialog, dialogcat;
    public static ImageView iv_grid;
    /////////////
    LinearLayout lv_newest;
    String cat_id;

    //
    //varible for pagination
    public static int page, pageno = 1;
    public static boolean isLoading = true;
    public static int pastvisibleitem, visibleitemcount, totalitemcount, previous_total = 0;
    public static int view_threshold = 10;
    public static NestedScrollView nestedscroll_shop;

    public static WrapContentLinearLayoutManager layoutManager;
    public static ProgressBar progress_shop_product;
    public static TextView tv_data_not_found_shop, tv_newestttt;
    public static LinearLayout lv_final_shop_click;

    public static String cat_iddd, order = "null", orderpass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_button_shop_activity);
        AllocateMemory();
        context = this;
        setTitle("Shop");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        category_list_models_tab.add(new Category_List_Model("0", "All"));


        cat_adapter = new Shop_subCategory_Adapter(Final_button_shop_activity.this, Home_frag.category_list_models_tab);
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(Final_button_shop_activity.this, LinearLayoutManager.HORIZONTAL, false);
        recv_category.setLayoutManager(layoutManager3);
        recv_category.setAdapter(cat_adapter);
        cat_id = getIntent().getStringExtra("cat_id");
        Log.e("cat_id_104", "" + cat_id);
        if (CheckNetwork.isNetworkAvailable(context)) {

            // CALL_CATEGORY_GRID_API();
            if (cat_id == "" || cat_id == null || cat_id == "null" || cat_id.equalsIgnoreCase(null)
                    || cat_id.equalsIgnoreCase("null")) {
                CALL_RETROFIT_SHOP_API(String.valueOf(0), 1, Main_Url.GRID_TYPE);
                // Log.e("nulll",""+cat_id);
            } else {
                CALL_RETROFIT_SHOP_API(cat_id, 1, Main_Url.GRID_TYPE);

            }
        } else {
            Toast.makeText(context, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        iv_grid.setOnClickListener(this);
        lv_newest.setOnClickListener(this);
    }


    public static void enableProgressBar_cat() {
        dialogcat = new Dialog(context);
        /*progressdialog.setMessage("Please Wait....");
         */
        dialogcat.setContentView(R.layout.custom);
        dialogcat.show();
        dialogcat.setCanceledOnTouchOutside(false);
    }

    public static void enableProgressBar() {
        dialog = new Dialog(context);
        /*progressdialog.setMessage("Please Wait....");
         */
        dialog.setContentView(R.layout.custom);
        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.myprogress);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    public static void CALL_RETROFIT_SHOP_API(final String category_id1, int page_no, final int data_show_type) {
        progress_shop_product.setVisibility(View.GONE);
        enableProgressBar();
        // lv_final_shop_click.setVisibility(View.GONE);
        page = page_no;
        cat_iddd = category_id1;

        Log.e("category_id_108", "" + category_id1);
        orderpass = order;

        Log.e("orderpass_157", "fbdb" + orderpass);
        itemArrayList.clear();
        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_library_list;

        if (category_id1.equals("0") == true || category_id1.equalsIgnoreCase("0") == true
                || category_id1 == "0") {
            Log.e("category_all_83", "" + category_id1);
            cat_iddd = "";
            //  get_library_list = api.get_Shop_list(String.valueOf(page), Main_Url.pass_per_page, "","");
        }
        if (orderpass == "" || orderpass == null || orderpass == "null" || orderpass.equalsIgnoreCase(null)
                || orderpass.equalsIgnoreCase("null")) {

            orderpass = "";
            Log.e("orderpass_blank", "gdfgfdg" + orderpass);
            get_library_list = api.get_Shop_list(String.valueOf(page), Main_Url.pass_per_page, cat_iddd);
        } else {
            Log.e("orderpass_177", "" + orderpass);
            get_library_list = api.get_Sort_list(String.valueOf(page), Main_Url.pass_per_page, cat_iddd, orderpass);
        }
        Log.e("orderpass_177", "" + orderpass);

        get_library_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                //  Log.e(" shop_retro_res", "" + response.body().toString());
                Log.e(" shop_retro_res", "" + response);
                // JSONObject jsonObject = new JSONObject(response.body().string());

                Log.e("api_cat_id_172", "" + category_id1);
                Log.e("res_shop_175", "" + response);

                itemArrayList.clear();
                recv_category_shop_data.setVisibility(View.VISIBLE);
                // tvNoDataFound.setVisibility(View.GONE);
                dialog.dismiss();
                // lv_final_shop_click.setVisibility(View.VISIBLE);
                JSONArray jsonArray = null;
                try {
                    Log.e("jsonArray", "" + jsonArray);

                    jsonArray = new JSONArray(response.body().string());
                    if (jsonArray != null && jsonArray.isNull(0) != true) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject json_object = jsonArray.getJSONObject(i);
                                JSONObject object = null;
                                JSONArray product_img_array = json_object.getJSONArray("images");
                                for (int j = 0; j < product_img_array.length(); j++) {
                                    object = product_img_array.getJSONObject(j);
                                    // Log.e("object_image", "" + object.getString("src"));
                                }
                                itemArrayList.add(new Shop_cat_model(
                                        object.getString("src"),
                                        json_object.getString("name"),
                                        json_object.getString("regular_price"),
                                        json_object.getString("price"),
                                        json_object.getString("name"),
                                        json_object.getString("name"),
                                        json_object.getString("id")));

                            } catch (Exception e) {
                                // Log.e("Exception", "aaaa=" + e);
                            } finally {
                                //    adapter.notifyItemChanged(i);
                            }
                        }
                        if (data_show_type == 0) {
                            Log.e("data_show_grid", "" + data_show_type);
                            adapter = new Shop_list_adapter(context, itemArrayList);
                            layoutManager = new WrapContentLinearLayoutManager(context, 2);
                            // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                            recv_category_shop_data.setLayoutManager(layoutManager);
                            recv_category_shop_data.setAdapter(adapter);

                        } else {
                            Log.e("data_show_verticle", "" + data_show_type);
                            adapter = new Shop_list_adapter(context, itemArrayList);
                            layoutManager = new WrapContentLinearLayoutManager(context, 1);
                            // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                            recv_category_shop_data.setLayoutManager(layoutManager);
                            recv_category_shop_data.setAdapter(adapter);

                        }

                        if (itemArrayList.size() == 0) {
                            tv_data_not_found_shop.setVisibility(View.VISIBLE);
                        } else {
                            tv_data_not_found_shop.setVisibility(View.GONE);
                        }
                    } else {
                        tv_data_not_found_shop.setVisibility(View.VISIBLE);
                        recv_category_shop_data.setVisibility(View.GONE);
                        tv_data_not_found_shop.setVisibility(View.VISIBLE);
                        // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Failure", "" + t.getLocalizedMessage());
            }
        });


        ////////////////////
        nestedscroll_shop.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        visibleitemcount = layoutManager.getChildCount();
                        totalitemcount = layoutManager.getItemCount();
                        pastvisibleitem = layoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {

                            if ((visibleitemcount + pastvisibleitem) >= totalitemcount) {
                                page++;
                                Log.e("isloading", "loadscroll" + page);
                                progress_shop_product.setVisibility(View.VISIBLE);
                                PerformPagination(category_id1, page, Main_Url.GRID_TYPE);
                                //  isLoading=true;
//                        Load Your Data
                            }
                        }
                    }
                }
            }
        });


    }

    public static void PerformPagination(final String category_idd, final int pagee, final int data_show_type) {
        page = pagee;
        cat_iddd = category_idd;

        Log.e("pagina", "" + category_idd);
        orderpass = order;

        Log.e("orderpass_157", "fbdb" + orderpass);

        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> get_library_list;

        if (category_idd.equals("0") == true || category_idd.equalsIgnoreCase("0") == true
                || category_idd == "0") {
            Log.e("category_all_83", "" + category_idd);
            cat_iddd = "";
            //  get_library_list = api.get_Shop_list(String.valueOf(page), Main_Url.pass_per_page, "","");
        }
        if (orderpass == "" || orderpass == null || orderpass == "null" || orderpass.equalsIgnoreCase(null)
                || orderpass.equalsIgnoreCase("null")) {

            orderpass = "";
            Log.e("orderpass_blank", "gdfgfdg" + orderpass);
            get_library_list = api.get_Shop_list(String.valueOf(page), Main_Url.pass_per_page, cat_iddd);
        } else {
            Log.e("orderpass_177", "" + orderpass);
            get_library_list = api.get_Sort_list(String.valueOf(page), Main_Url.pass_per_page, cat_iddd, orderpass);
        }
        Log.e("orderpass_177", "" + orderpass);

        get_library_list.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                //  Log.e(" shop_retro_res", "" + response.body().toString());
                Log.e(" shop_retro_res", "" + response);
                // JSONObject jsonObject = new JSONObject(response.body().string());

                Log.e("api_cat_id_172", "" + category_idd);
                Log.e("res_shop_pagina", "" + response);

                progress_shop_product.setVisibility(View.GONE);
                recv_category_shop_data.setVisibility(View.VISIBLE);
                // tvNoDataFound.setVisibility(View.GONE);
                // lv_final_shop_click.setVisibility(View.VISIBLE);
                JSONArray jsonArray = null;
                try {
                    Log.e("jsonArray", "" + jsonArray);

                    jsonArray = new JSONArray(response.body().string());
                    if (jsonArray != null && jsonArray.isNull(0) != true) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject json_object = jsonArray.getJSONObject(i);
                                JSONObject object = null;
                                JSONArray product_img_array = json_object.getJSONArray("images");
                                for (int j = 0; j < product_img_array.length(); j++) {
                                    object = product_img_array.getJSONObject(j);
                                    // Log.e("object_image", "" + object.getString("src"));
                                }
                                itemArrayList.add(new Shop_cat_model(
                                        object.getString("src"),
                                        json_object.getString("name"),
                                        json_object.getString("regular_price"),
                                        json_object.getString("price"),
                                        json_object.getString("name"),
                                        json_object.getString("name"),
                                        json_object.getString("id")));

                            } catch (Exception e) {
                                // Log.e("Exception", "aaaa=" + e);
                            } finally {
                                //    adapter.notifyItemChanged(i);
                            }
                        }
                        if (data_show_type == 0) {
                            Log.e("data_show_grid", "" + data_show_type);
                            adapter = new Shop_list_adapter(context, itemArrayList);
                            layoutManager = new WrapContentLinearLayoutManager(context, 2);
                            // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                            recv_category_shop_data.setLayoutManager(layoutManager);
                            recv_category_shop_data.setAdapter(adapter);

                        } else {
                            Log.e("data_show_verticle", "" + data_show_type);
                            adapter = new Shop_list_adapter(context, itemArrayList);
                            layoutManager = new WrapContentLinearLayoutManager(context, 1);
                            // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                            recv_category_shop_data.setLayoutManager(layoutManager);
                            recv_category_shop_data.setAdapter(adapter);

                        }

                        if (itemArrayList.size() == 0) {
                            tv_data_not_found_shop.setVisibility(View.VISIBLE);
                        } else {
                            tv_data_not_found_shop.setVisibility(View.GONE);
                        }
                    } else {
                        //tv_data_not_found_shop.setVisibility(View.VISIBLE);
                      //  recv_category_shop_data.setVisibility(View.GONE);
                        //tv_data_not_found_shop.setVisibility(View.VISIBLE);
                        // Toast.makeText(Sub_category.this, "Locations not Available", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("Failure", "" + t.getLocalizedMessage());
            }
        });


    }


    private void AllocateMemory() {
        lv_final_shop_click = (LinearLayout) findViewById(R.id.lv_final_shop_click);

        iv_grid = (ImageView) findViewById(R.id.iv_grid);
        recv_category = (RecyclerView) findViewById(R.id.recv_category);
        recv_category_shop_data = (RecyclerView) findViewById(R.id.recv_category_shop_data);
        progress_shop_product = (ProgressBar) findViewById(R.id.progress_shop_product);
        nestedscroll_shop = (NestedScrollView) findViewById(R.id.nestedscroll_shop);
        tv_data_not_found_shop = (TextView) findViewById(R.id.tv_data_not_found_shop);
        tv_newestttt = (TextView) findViewById(R.id.tv_newestttt);
        lv_newest = (LinearLayout) findViewById(R.id.lv_newest);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cart, menu);
/*
        Log.e("countsubjava", "" + count1);
        MenuItem menuItem = menu.findItem(R.id.wish);
        icon = (LayerDrawable) menuItem.getIcon();

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(Sub_category.this);
        }
        // Log.e("countt",""+count);
        if (count1 == "" || count1 == null || count1 == "null" || count1.equalsIgnoreCase(null)
                || count1.equalsIgnoreCase("null")) {
            count1 = String.valueOf(0);
            Log.e("countt", "" + count1);
        }
        badge.setCount(count1);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
/*
            Log.e("checkscreen_else", "" + CheckScreen);
            onBackPressed();*/
        } else if (item.getItemId() == R.id.cart) {

            startActivity(new Intent(Final_button_shop_activity.this, Cart_Activity.class));
        }       // Toast.makeText(getApplicationContext(), "wishlist", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == iv_grid) {
            if (Main_Url.GRID_TYPE == 0) {
                iv_grid.setImageResource(R.drawable.ic_md_list);

                Log.e("data_show_verticle_btn", "grid_to_verticle");
                adapter = new Shop_list_adapter(context, itemArrayList);
                layoutManager = new WrapContentLinearLayoutManager(context, 1);
                // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                recv_category_shop_data.setLayoutManager(layoutManager);
                recv_category_shop_data.setAdapter(adapter);
                Main_Url.GRID_TYPE = 2;
                // Toast.makeText(context,"iv grid is clicked" + Main_Url.GRID_TYPE,Toast.LENGTH_LONG).show();

                // CALL_PRODUCT_ALL_VERTICAL_LIST_API(page_all);

            } else if (Main_Url.GRID_TYPE == 2) {
                iv_grid.setImageResource(R.drawable.ic_md_apps);

                Log.e("data_show_grid_btn", "changes_verticla_to_grid");
                adapter = new Shop_list_adapter(context, itemArrayList);
                layoutManager = new WrapContentLinearLayoutManager(context, 2);
                // recycler_shop_product.setLayoutManager(new GridLayoutManager(Shop_Activity.this, 2));
                recv_category_shop_data.setLayoutManager(layoutManager);
                recv_category_shop_data.setAdapter(adapter);
                Main_Url.GRID_TYPE = 0;


                ///                  Toast.makeText(context,"iv list is clicked" + Main_Url.GRID_TYPE,Toast.LENGTH_LONG).show();
//                    CALLGRIDAPI();
            } else {
                Toast.makeText(context, "grid view in else part", Toast.LENGTH_LONG).show();
            }


        } else if (view == lv_newest) {
            final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(Final_button_shop_activity.this);
            View dialogView = Final_button_shop_activity.this.getLayoutInflater().inflate(R.layout.dialog_bottom_sheet, null);
//                Button btn_dialog_bottom_sheet_ok = dialogView.findViewById(R.id.btn_dialog_bottom_sheet_ok);
            //Button btn_dialog_bottom_sheet_cancel = dialogView.findViewById(R.id.btn_dialog_bottom_sheet_cancel);
            ImageView img_bottom_dialog = dialogView.findViewById(R.id.img_bottom_dialog);
            TextView cancel = dialogView.findViewById(R.id.tv_cancel);
            final TextView tv_newest = dialogView.findViewById(R.id.tv_newest);
            TextView tv_a_to_z = dialogView.findViewById(R.id.tv_a_to_z);
            TextView tv_z_to_a = dialogView.findViewById(R.id.tv_z_to_a);

            tv_a_to_z.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("category_id_155", "" + cat_iddd);
                    order = "asc";
                    CALL_RETROFIT_SHOP_API(cat_iddd, page, Main_Url.GRID_TYPE);

                    tv_newestttt.setText("A to Z");
                    mBottomSheetDialog.dismiss();

                }
            });
            tv_newest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CALL_RETROFIT_SHOP_API(cat_iddd, page, Main_Url.GRID_TYPE);

                    tv_newestttt.setText("Newest");
                    mBottomSheetDialog.dismiss();
                }
            });

            tv_z_to_a.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    order = "desc";
                    CALL_RETROFIT_SHOP_API(cat_iddd, page, Main_Url.GRID_TYPE);


                    tv_newestttt.setText("Z to A");
                    mBottomSheetDialog.dismiss();
                }
            });

            // Glide.with(Shop_Activity.this).load(R.drawable.bottom_dialog).into(img_bottom_dialog);
            mBottomSheetDialog.setContentView(dialogView);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();
                }
            });
            mBottomSheetDialog.show();

        }
    }

    private void CALL_SORT_API() {
    }

    public void CALL_CATEGORY_GRID_API() {
        final String CAT_URL = Main_Url.CATEGORY_URL + "&page=" + pageno + "&per_page=" + Main_Url.per_page
                + "&hide_empty=" + Main_Url.hide_empty;
        Log.e("url_cat_62", "" + CAT_URL);

/*
        if(pageno==2)
        {
            dialogcat.dismiss();
            lv_final_shop_click.setVisibility(View.VISIBLE);
        }else {
            enableProgressBar_cat();

        }*/

        //  enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CAT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_CATURL_response", "" + CAT_URL);
                        Log.e("response", "" + response);
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("response_jsonarray", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                      /*  if (jsonObject.getString("parent").equals("0") == true) {
                                            JSONObject image_obj = jsonObject.getJSONObject("image");
                                            Category_Model gmodel = new Category_Model
                                                    (jsonObject.getString("name"),
                                                            image_obj.getString("src")
                                                            , jsonObject.getString("id")
                                                            , jsonObject.getString("parent"));
                                            category_grid_model.add(gmodel);

                                        }
                                      */
                                        String text = String.valueOf(Html.fromHtml(jsonObject.getString("name")));
                                        Category_List_Model model = new Category_List_Model
                                                (jsonObject.getString("id"), text
                                                );
                                        category_list_models_tab.add(model);

                                        String title = jsonObject.getString("name");
                                        if (title.equals("Offers") || title == "Offers") {
                                            int offerpos = i;
                                            //       Log.e("offerpos",offerpos+"");
                                        }
                                    } catch (Exception e) {
                                        //Log.e("Exception", "" + e);
                                    } finally {
                                    }
                                }


                                //-------call again grid api----------//

                                pageno = pageno + 1;
                                CALL_CATEGORY_GRID_API();


                            } else {
                                pageno = 1;
                                Log.e("flag", "flag");
                                Log.e("Json_array_147", "null" + jsonArray);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(Final_button_shop_activity.this);
        requestQueue.add(stringRequest);

    }

}

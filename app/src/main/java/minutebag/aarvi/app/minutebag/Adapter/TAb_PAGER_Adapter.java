package minutebag.aarvi.app.minutebag.Adapter;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import minutebag.aarvi.app.minutebag.Fragment.New_dynamic_Library_frag;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.Model.Category_Model;

public class TAb_PAGER_Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<Category_List_Model> mFragmentTitleList = new ArrayList<>();

        boolean visible;

        public TAb_PAGER_Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return New_dynamic_Library_frag.newInstance(
                    mFragmentTitleList.get(position).getCategory_id(), visible,
                    mFragmentTitleList.get(position).getCategory_title());
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, Category_List_Model title) {

            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position).getCategory_title();
        }
    }


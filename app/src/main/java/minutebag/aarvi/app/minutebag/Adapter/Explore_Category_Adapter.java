package minutebag.aarvi.app.minutebag.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Model.Cat_Explore_Model;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;

public class Explore_Category_Adapter extends RecyclerView.Adapter<Explore_Category_Adapter.MyViewHolder> {
    private List<Cat_Explore_Model> category_models;
    private Context context;
    LayoutInflater inflater;
    public static String cat_id;
    int selectedPosition ;
    public static int page_no = 1;
    public int mSelectedItem = -1;

    public Explore_Category_Adapter(Context context, List<Cat_Explore_Model> product_model) {
        this.context = context;
        this.category_models = product_model;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedPosition=0;
    }

    @Override
    public Explore_Category_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.explore_product_row, parent, false);
        return new Explore_Category_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Explore_Category_Adapter.MyViewHolder holder, final int position) {
        final Cat_Explore_Model model = category_models.get(position);
       // holder.tv_name.setText(Html.fromHtml(model.getCat_title()));
        //Glide.with(context).load(model.getCat_image()).into(holder.iv_image);
        Navigation_activity.Check_String_NULL_Value(holder.tv_name,model.getCat_title());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.drawable_ldpi_icon);
        requestOptions.error(R.drawable.drawable_ldpi_icon);
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(model.getCat_image()).into(holder.iv_image);
        Log.e("adaoter_home_position", "" + position);

       /* if (position == 0) {
            Log.e("cattitleeee", "" + category_models.get(position).getCat_title());
            Log.e("home_position", "" + position);

            Home_frag.CALL_EXPLORE_PRODUCT_ALL_API(page_no);
            Glide.with(context).load(R.drawable.ic_md_apps).into(holder.iv_image);

        }*/
        if(selectedPosition==position)
        {
            Login_preference.setcatid(context, cat_id);
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.view.setVisibility(View.VISIBLE);

        }else {
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.black));
            holder.view.setVisibility(View.GONE);

        }
        holder.lv_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_id = category_models.get(position).getCat_id();
                Log.e("iddddddddddddd", "" + category_models.get(position).getCat_id());
                Home_frag.CALL_EXPLORED_PRODUCT_API(cat_id, page_no);
                mSelectedItem = position;
                notifyDataSetChanged();


            }
        });

        if (mSelectedItem == position) {
            Login_preference.setcatid(context, cat_id);
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.view.setVisibility(View.VISIBLE);
        } else {
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.black));
            holder.view.setVisibility(View.GONE);

        }

    }


    @Override
    public int getItemCount() {
        return category_models.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public ImageView iv_image;
        LinearLayout lv_click;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);

            view = (View) itemView.findViewById(R.id.view);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            lv_click = (LinearLayout) itemView.findViewById(R.id.lv_click);
            iv_image = (ImageView) itemView.findViewById(R.id.iv_image);
        }
    }
}





package minutebag.aarvi.app.minutebag.Adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Activity.Product_Detail;
import minutebag.aarvi.app.minutebag.Fragment.Sub_Category_freg;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.R;


public class Product_Latest_home_adapter extends RecyclerView.Adapter<Product_Latest_home_adapter.MyViewHolder> {

    int product_quantity=0;
    public  static CardView card_product;
    private List<Shop_cat_model> product_model;
        private Context context;
        LayoutInflater inflater;

        public Product_Latest_home_adapter(Context context, List<Shop_cat_model> product_model) {
            this.context = context;
            this.product_model = product_model;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.product_row, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            final Shop_cat_model model = product_model.get(position);
           // holder.tv_product_title.setText(model.getProduct_name());
           // holder.tv_new_price.setText(context.getResources().getString(R.string.rs)+model.getProduct_new_price());
           // holder.tv_old_price.setText(context.getResources().getString(R.string.rs)+model.getProduct_old_price());
            Navigation_activity.Check_String_NULL_Value(holder.tv_product_title,model.getProduct_name());
            Navigation_activity.Check_String_NULL_Value(holder.tv_new_price,context.getResources().getString(R.string.rs) + model.getProduct_new_price());
            Navigation_activity.Check_String_NULL_Value(holder.tv_old_price,context.getResources().getString(R.string.rs) + model.getProduct_old_price());
            holder.tv_old_price.setPaintFlags(holder.tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            holder.lv_variable_spinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

            /*    dialog.setContentView(R.layout.custom_dialog_shop);
                dialog.show();*/
                    Dialog dialog=new Dialog(context);
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    Rect displayRectangle = new Rect();
                    Window window = activity.getWindow();
                    window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

// inflate and adjust layout
                    LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater.inflate(R.layout.custom_dialog_shop, null);
                    layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
                    layout.setMinimumHeight((int)(displayRectangle.height() * 0.4f));
                    dialog.setContentView(layout);
                    dialog.show();
                }
            });


            holder.lv_add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    holder.lv_add_to_cart.setVisibility(View.GONE);
                    holder.lv_add_quantity.setVisibility(View.VISIBLE);
                    holder.lv_increse.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            product_quantity=product_quantity+1;
               //             Log.e("product_104",""+product_quantity);
                            String result= String.valueOf(product_quantity);
                            holder.tv_product_quantity.setText(result);
                        }
                    });
                    holder.lv_decrese.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(product_quantity!=0) {
                                product_quantity -= 1;
                                String result = String.valueOf(product_quantity);
             //                   Log.e("product_1011_minus", "" + product_quantity);
                                holder.tv_product_quantity.setText(result);
                            }else {
                                holder.tv_product_quantity.setText( String.valueOf(product_quantity));
                            }
                        }
                    });
                }
            });

            holder.lv_product_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    //Bundle id_bundle=new Bundle();
                    //id_bundle.putString("cat_id",model.getCat_id());
                    //Fragment myFragment = new Sub_Category_freg();
                   // myFragment.setArguments(id_bundle);
                    Intent i=new Intent(context, Product_Detail.class);
                    i.putExtra("product_id",model.getProduct_id());
                    activity.startActivity(i);

                   // activity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, myFragment).addToBackStack(null).commit();


                }
            });
            RequestOptions requestOptions1 = new RequestOptions();
            requestOptions1.placeholder(R.drawable.drawable_ldpi_icon);
            requestOptions1.error(R.drawable.drawable_ldpi_icon);
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions1)
                    .load(model.getProduct_img()).into(holder.iv_product);


            //Glide.with(context).load(model.getProduct_img()).into(holder.iv_product);


/*
            holder.ll_cat_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AppCompatActivity activity = (AppCompatActivity) view.getContext();

                            cat_id=category_model.get(position).getCategory_id();
                            Log.e("id",""+cat_id);
                            cat_name=category_model.get(position).getTxtcatnm();
                            Log.e("catname",""+cat_name);
                            Intent i=new Intent(context, Sub_category.class);
                            i.putExtra("cat_id",cat_id);
                            i.putExtra("catname",cat_name);
                            activity.startActivity(i);
                            activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                        }
                    }, 50);



                }
            });
*/
        }



        @Override
        public int getItemCount() {
            return product_model.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView iv_product,iv_wishlist;

            LinearLayout lv_product_click,lv_add_to_cart,lv_variable_spinner,lv_decrese,lv_increse,lv_add_quantity;
            TextView tv_save_rs,tv_product_title,tv_add_to_cart,tv_old_price,tv_new_price,tv_product_quantity,tv_minus,tv_add;
            public MyViewHolder(View itemView) {
                super(itemView);
                iv_product = (ImageView) itemView.findViewById(R.id.iv_product);
                iv_wishlist = (ImageView) itemView.findViewById(R.id.iv_wishlist);
                card_product = (CardView) itemView.findViewById(R.id.card_product);

                lv_product_click = (LinearLayout) itemView.findViewById(R.id.lv_product_click);
                lv_add_to_cart = (LinearLayout) itemView.findViewById(R.id.lv_add_to_cart);
                lv_decrese = (LinearLayout) itemView.findViewById(R.id.lv_decrese);
                lv_increse = (LinearLayout) itemView.findViewById(R.id.lv_increse);
                lv_add_quantity = (LinearLayout) itemView.findViewById(R.id.lv_add_quantity);
                lv_variable_spinner = (LinearLayout) itemView.findViewById(R.id.lv_variable_spinner);

                tv_add = (TextView) itemView.findViewById(R.id.tv_add);
                tv_minus = (TextView) itemView.findViewById(R.id.tv_minus);
                tv_product_quantity = (TextView) itemView.findViewById(R.id.tv_product_quantity);
                tv_save_rs = (TextView) itemView.findViewById(R.id.tv_save_rs);
                tv_product_title = (TextView) itemView.findViewById(R.id.tv_product_title);
                tv_old_price = (TextView) itemView.findViewById(R.id.tv_old_price);
                tv_new_price = (TextView) itemView.findViewById(R.id.tv_new_price);
                tv_add_to_cart = (TextView) itemView.findViewById(R.id.tv_add_to_cart);
            }
        }
    }



package minutebag.aarvi.app.minutebag.Preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Login_preference {
    public static SharedPreferences mPrefs;
    public static SharedPreferences.Editor prefsEditor;

//check_screen
    public static void setscreen(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("screen", value);
        prefsEditor.commit();
    }
    public static String getscreen(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("screen", "");
    }

    public static void setcatid(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("catid", value);
        prefsEditor.commit();
    }
    public static String getcatid(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("catid", "");
    }

    ///Logindata
    public static void setcustomer_id(Context context, String value)
   {
       mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
       prefsEditor = mPrefs.edit();
       prefsEditor.putString("customer_id", value);
       prefsEditor.commit();
   }
    public static String getcustomer_id(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("customer_id", "");
    }

    public static void  set_wishlist_value(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("wishlist", value);
        prefsEditor.commit();
    }
    public static String get_wishlist_value(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("wishlist", "");
    }
    public static void setEmailid(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("email", value);
        prefsEditor.commit();
    }
    public static String getEmailid(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("email", "");
    }

    public static void setMobileno(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("Mobile", value);
        prefsEditor.commit();
    }
    public static String getmobileno(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("Mobile", "");
    }

    public static void setfirstname(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("firstname", value);
        prefsEditor.commit();
    }
    public static String getfirstname(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("firstname", "");
    }

    public static void setLastname(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("Lastname", value);
        prefsEditor.commit();
    }
    public static String getLastname(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("Lastname", "");
    }
    public static void setgender(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("gender", value);
        prefsEditor.commit();
    }
    public static String getgender(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("gender", "");
    }
    public static void setProfilepicture(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("profilepic", value);
        prefsEditor.commit();
    }
    public static String getProfilepicture(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("profilepic", "");
    }
    public static void setMeritalStaus(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("mstatus", value);
        prefsEditor.commit();
    }
    public static String getMeritalStaus(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("mstatus", "");
    }

    public static void setdate_of_birth(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("dob", value);
        prefsEditor.commit();
    }
    public static String getdate_of_birth(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("mstatus", "");
    }

    public static void setAnnive_date(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("date", value);
        prefsEditor.commit();
    }
    public static String getAnnive_date(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("date", "");
    }

    public static void setloginflag(Context context, String value)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPrefs.edit();
        prefsEditor.putString("flag", value);
        prefsEditor.commit();
    }
    public static String getloginflag(Context context)
    {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("flag", "");
    }



    ///set fb data in sharedpreference


}

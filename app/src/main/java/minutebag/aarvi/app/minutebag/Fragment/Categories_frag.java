package minutebag.aarvi.app.minutebag.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.renderscript.BaseObj;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import minutebag.aarvi.app.minutebag.Adapter.Category_grid_adapter;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.CheckNetwork;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class Categories_frag extends Fragment {
   // public static ArrayList<Category_Model> grid_model = new ArrayList<Category_Model>();
    public static GridView gridview;
    ProgressBar progress_grid;
    FrameLayout frame_grid;
    public static int PAGE = 1;
    public static Category_grid_adapter category_grid_adapter;
    public static String CAT_URL;
    public static Boolean FLAG = true;
    private static Context context = null;

    public static ProgressDialog progressdialog;
    View v;

    public Categories_frag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_categories_frag, container, false);
        context = getActivity();
        allocatememory(v);

       // grid_model.clear();
      //  CALL_GRID_API(gridview);
        Log.e("75===",""+Home_frag.category_grid_model);
        category_grid_adapter = new Category_grid_adapter(getContext(),Home_frag. category_grid_model);
        gridview.setAdapter(category_grid_adapter);


        return v;
    }

  /*  public  void CALL_GRID_API(final GridView gridView_cat) {
        CAT_URL = Main_Url.CATEGORY_URL + "&page=" + PAGE + "&per_page=" + Main_Url.per_page
                + "&hide_empty=" + Main_Url.hide_empty;
        Log.e("url_cat_62", "" + CAT_URL);
        //enableProgressBar();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CAT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_CATURL_response", "" + CAT_URL);
                        Log.e("response", "" + response);
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("response_jsonarray", "" + jsonArray);
                            if (jsonArray != null && jsonArray.isNull(0) != true) {
                                Log.e("jsonarray_105", "" + jsonArray);
                                Log.e("jsonarray_105Flag", "" + FLAG);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        Log.e("object", "" + jsonObject);
                                        Log.e("data", "" + jsonObject.getString("parent"));
                                        if (jsonObject.getString("parent").equals("0") == true) {

                                            Log.e("data", "" + jsonObject.getString("parent"));
                                            Category_Model gmodel = new Category_Model
                                                    (jsonObject.getString("name"),
                                                            jsonObject.getString("image")
                                                            , jsonObject.getString("id")
                                                            , jsonObject.getString("parent"));
                                            grid_model.add(gmodel);

                                        } else {
                                        }
                                    } catch (Exception e) {
                                        Log.e("Exception", "" + e);
                                    } finally {
                                    }
                                }

                                category_grid_adapter = new Category_grid_adapter(getContext(), grid_model);
                                gridView_cat.setAdapter(category_grid_adapter);

                                //-------call again grid api----------//

                                PAGE = PAGE + 1;
                                CALL_GRID_API(gridView_cat);


                            } else {
                                PAGE = 1;
          //                      progressdialog.dismiss();
                                Log.e("flag", "flag");
                                Log.e("Json_array_147", "null" + jsonArray);
                            }

                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }
*/
    public  void enableProgressBar() {
        progressdialog = new ProgressDialog(getActivity());
        /*progressdialog.setMessage("Please Wait....");
         */
        progressdialog.show();
        progressdialog.setCancelable(false);
    }


    private void allocatememory(View v) {
        gridview = (GridView) v.findViewById(R.id.gridview);
        progress_grid = (ProgressBar) v.findViewById(R.id.progress_grid);
        //  frame_grid = (FrameLayout)v.findViewById(R.id.frame_grid);
    }

}

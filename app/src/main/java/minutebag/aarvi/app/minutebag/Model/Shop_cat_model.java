package minutebag.aarvi.app.minutebag.Model;

public class Shop_cat_model {

String product_img;
    String product_name;
    String product_old_price;
    String product_new_price;
    String wishlist;
    String product_offers_save;
    String product_id;
    int Increase;

    public int getIncrease() {
        return Increase;
    }

    public void setIncrease(int increase) {
        Increase = increase;
    }


    public Shop_cat_model(String product_img,
                          String product_name, String product_old_price,
                          String product_new_price,
                          String wishlist, String product_offers_save, String product_id) {
        this.product_img = product_img;
        this.product_name = product_name;
        this.product_old_price = product_old_price;
        this.product_new_price = product_new_price;
        this.wishlist = wishlist;
        this.product_offers_save = product_offers_save;
        this.product_id = product_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }


    public String getProduct_img() {

        return product_img;
    }

    public void setProduct_img(String product_img) {
        this.product_img = product_img;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_old_price() {
        return product_old_price;
    }

    public void setProduct_old_price(String product_old_price) {
        this.product_old_price = product_old_price;
    }

    public String getProduct_new_price() {
        return product_new_price;
    }

    public void setProduct_new_price(String product_new_price) {
        this.product_new_price = product_new_price;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getProduct_offers_save() {
        return product_offers_save;
    }

    public void setProduct_offers_save(String product_offers_save) {
        this.product_offers_save = product_offers_save;
    }
}

package minutebag.aarvi.app.minutebag.Activity;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import minutebag.aarvi.app.minutebag.Fragment.Categories_frag;
import minutebag.aarvi.app.minutebag.Fragment.ContactUs;
import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Fragment.Settings_frag;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;

public class Navigation_activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    boolean doubleBackToExitPressedOnce = false;

    private BottomNavigationView bottom_navigation;
    public static ImageView iv_headerlogo;
    private List<View> viewList;
    public  static  TextView tv_title;
    Toolbar toolbar;
    DrawerLayout drawer;
    LinearLayout header;
    TextView txtnavusername,txtnavemail;
    NavigationView navigationView;
    Context ctx=this;
    ImageView iv_home_search,iv_home_cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        setSupportActionBar(toolbar);
        AllocateMemory();

        if (Login_preference.getcustomer_id(ctx).length() > 0)
        {
            txtnavemail.setText(Login_preference.getEmailid(ctx));
            String first_name,last_name;
            first_name=Login_preference.getfirstname(ctx);
            last_name=Login_preference.getLastname(ctx);
            txtnavusername.setText(first_name + "  " + last_name);

            header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager headmanager = getSupportFragmentManager();
                    headmanager.beginTransaction().replace(R.id.frame_layout, new Settings_frag()).commit();
                    tv_title.setText(R.string.title_setting);
                    iv_headerlogo.setVisibility(View.GONE);
                    tv_title.setVisibility(View.VISIBLE);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
        }
        else
        {
            txtnavemail.setText("Please login or create an account for free");
            txtnavusername.setText("login and Register");
            header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Navigation_activity.this, Login_activity.class));
                    // Toast.makeText(HomeActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        iv_home_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation_activity.this, Search_activity.class));
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            }
        });
        iv_home_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation_activity.this, Cart_Activity.class));
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            }
        });
///////////////////////////////////////
        //bottomnavigation
        Window window = getWindow();
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.argb(33, 0, 0, 0));
        }
        initView();

        //-----By defulat open fregment-------//
        pushFragment(new Home_frag());
        iv_headerlogo.setVisibility(View.VISIBLE);

        ////-----navigation header clicklistner---------//

    }

    private void AllocateMemory() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_headerlogo = (ImageView) findViewById(R.id.iv_headerlogo);
        iv_home_search = (ImageView) findViewById(R.id.iv_home_search);
        iv_home_cart = (ImageView) findViewById(R.id.iv_home_cart);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerview = navigationView.getHeaderView(0);
        txtnavemail = (TextView) headerview.findViewById(R.id.txtnavemail);
        txtnavusername = (TextView) headerview.findViewById(R.id.txtnavusername);
        header = (LinearLayout) headerview.findViewById(R.id.header);

    }

    private void initView() {

        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public static void Check_String_NULL_Value( TextView textview, String text) {


        if(text.equalsIgnoreCase("null")==true)
        {
            textview.setText("");
        }else {

            textview.setText(Html.fromHtml(Navigation_activity.Convert_String_First_Letter(text)));
        }

    }
    public static String  Convert_String_First_Letter(String convert_string)
    {
        String upperString ;

        if(convert_string.length() > 0)
        {
            upperString = convert_string.substring(0,1).toUpperCase() + convert_string.substring(1);
        }else {
            upperString=" ";
        }
        return upperString;
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    pushFragment(new Home_frag());
                    //   tv_title.setText(R.string.title_home);
                  // iv_headerlogo.setVisibility(View.VISIBLE);
                    tv_title.setVisibility(View.GONE);
                    return true;
                case R.id.category:
                    pushFragment(new Categories_frag());
                    tv_title.setText(R.string.title_category);
                    iv_headerlogo.setVisibility(View.GONE);
                    tv_title.setVisibility(View.VISIBLE);
                    return true;
                case R.id.shop_category_all:
                    startActivity(new Intent(Navigation_activity.this, Final_button_shop_activity.class));
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    iv_headerlogo.setVisibility(View.GONE);
                    //tv_title.setVisibility(View.VISIBLE);
                    /* tv_title.setText("Shop");*/
                    iv_headerlogo.setVisibility(View.GONE);

                    return true;
                case R.id.offers:
                    Intent intent=new Intent(Navigation_activity.this,Final_button_shop_activity.class);
                    intent.putExtra("screen","offers");
                    startActivity(intent);
                 //   startActivity(new Intent(Navigation_activity.this, Shop_Activity.class));
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    iv_headerlogo.setVisibility(View.GONE);
                   // tv_title.setText(R.string.title_offers);
                    tv_title.setVisibility(View.VISIBLE);
                    return true;
                case R.id.settings:
                    PackageManager packageManager = getApplicationContext().getPackageManager();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    try {
                        String url = "https://api.whatsapp.com/send?phone=+919734615161&text=" + URLEncoder.encode("Please Chat", "UTF-8");
                        i.setPackage("com.whatsapp");
                        i.setData(Uri.parse(url));
                        if (i.resolveActivity(packageManager) != null) {
                            getApplicationContext().startActivity(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return true;
            }
            return false;
        }
    };


    private void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (ft != null) {
                ft.replace(R.id.frame_layout, fragment);
                ft.setCustomAnimations(R.anim.slide_from_right,R.anim.slide_from_left);
                ft.addToBackStack(null);
                ft.commit();
            }
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            pushFragment(new Home_frag());
            iv_headerlogo.setVisibility(View.VISIBLE);
            tv_title.setVisibility(View.GONE);
            drawer.closeDrawer(GravityCompat.START);
        } else if (id == R.id.nav_category) {
            pushFragment(new Categories_frag());

            tv_title.setText(R.string.title_category);
            iv_headerlogo.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_shop) {

            startActivity(new Intent(Navigation_activity.this, Final_button_shop_activity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            tv_title.setText(R.string.title_shop);
            iv_headerlogo.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_my_wishlist) {
            startActivity(new Intent(Navigation_activity.this, Final_button_shop_activity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            tv_title.setText("My Wishlist");
            iv_headerlogo.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_wallet) {
            startActivity(new Intent(Navigation_activity.this, Wallet_Activity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        } else if (id == R.id.nav_edit_profile) {
            startActivity(new Intent(Navigation_activity.this, Edit_Profile_activity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        }
        else if (id == R.id.nav_about_us) {
            startActivity(new Intent(Navigation_activity.this, About_Us.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        } else if (id == R.id.nav_share) {
            startActivity(new Intent(Navigation_activity.this, Edit_Profile_activity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            tv_title.setText("Share");
        } else if (id == R.id.nav_rate_us) {

        } else if (id == R.id.nav_settings) {
            pushFragment(new Settings_frag());
            tv_title.setText(R.string.title_setting);
            iv_headerlogo.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_contact_us) {
            pushFragment(new ContactUs());

            tv_title.setText("Contact Us");
            iv_headerlogo.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);

            }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }/*
    @Override
    public void onBackPressed() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        //  fragmentManager.popBackStack();
        //Here we are clearing back stack fragment entries

        int count = fragmentManager.getBackStackEntryCount();
        *//* if (count == 0) {*//*

        if (doubleBackToExitPressedOnce) {
            Log.e("198",""+doubleBackToExitPressedOnce);
            super.onBackPressed();
            super.finish();
            return;
        }
        Log.e("203",""+doubleBackToExitPressedOnce);
        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("208",""+doubleBackToExitPressedOnce);
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
       *//* }else {

            if (doubleBackToExitPressedOnce) {
                Log.e("198",""+doubleBackToExitPressedOnce);
                super.onBackPressed();
                super.finish();
                return;
            }
            Log.e("203",""+doubleBackToExitPressedOnce);
            this.doubleBackToExitPressedOnce = true;

            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("208",""+doubleBackToExitPressedOnce);
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);
        }*//*
      *//*  if (count == 1) {

            Log.e("196",""+doubleBackToExitPressedOnce);
            if (doubleBackToExitPressedOnce) {
                Log.e("198",""+doubleBackToExitPressedOnce);
                super.onBackPressed();
                super.finish();
                return;
            }
            Log.e("203",""+doubleBackToExitPressedOnce);
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("208",""+doubleBackToExitPressedOnce);
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);
        }
        else {
            // String title = fragmentManager.getBackStackEntryAt(count - 2).getName();

            super.onBackPressed();
            int countttt = fragmentManager.getBackStackEntryCount();
            Log.e("onBackPressetitle", "aaa" );
            Log.e("220_count_onBack", "aaa" +countttt);

            //tv_title.setText(title);
        }*//**//*else if (count == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                super.finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }*//*
    }



*/

}

package minutebag.aarvi.app.minutebag.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import minutebag.aarvi.app.minutebag.Adapter.Cart_Adapter;
import minutebag.aarvi.app.minutebag.Adapter.Shop_list_adapter;
import minutebag.aarvi.app.minutebag.Adapter.Shop_subCategory_Adapter;
import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Model.AddToCart;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.R;

public class Cart_Activity extends AppCompatActivity {
    LinearLayout lv_proceed_cart;
    RecyclerView cart_recycler;
    Cart_Adapter cart_adapter;
    public static ArrayList<AddToCart> addtocart_list = new ArrayList<AddToCart>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        setTitle("My Cart");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lv_proceed_cart=(LinearLayout)findViewById(R.id.lv_proceed_cart);
        cart_recycler=(RecyclerView) findViewById(R.id.cart_recycler);
        lv_proceed_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Cart_Activity.this,Shipping_address.class));
            }
        });


        Log.e("cartlist_44",""+Shop_list_adapter.cartList);
        cart_adapter = new Cart_Adapter(Cart_Activity.this,Shop_list_adapter.cartList);
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(Cart_Activity.this, LinearLayoutManager.VERTICAL, false);
        cart_recycler.setLayoutManager(layoutManager3);
        cart_recycler.setAdapter(cart_adapter);


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cart, menu);
/*
        Log.e("countsubjava", "" + count1);
        MenuItem menuItem = menu.findItem(R.id.wish);
        icon = (LayerDrawable) menuItem.getIcon();

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(Sub_category.this);
        }
        // Log.e("countt",""+count);
        if (count1 == "" || count1 == null || count1 == "null" || count1.equalsIgnoreCase(null)
                || count1.equalsIgnoreCase("null")) {
            count1 = String.valueOf(0);
            Log.e("countt", "" + count1);
        }
        badge.setCount(count1);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
/*
            Log.e("checkscreen_else", "" + CheckScreen);
            onBackPressed();*/
        }       // Toast.makeText(getApplicationContext(), "wishlist", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }
}

package minutebag.aarvi.app.minutebag.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import minutebag.aarvi.app.minutebag.Activity.Final_button_shop_activity;
import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Model.Category_List_Model;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class Shop_subCategory_Adapter extends RecyclerView.Adapter<Shop_subCategory_Adapter.MyViewHolder> {
    //private List<Cat_Explore_Model> category_models;
    public static List<Category_List_Model> subcat_model;
    public int mSelectedItem = -1;

    private Context context;
    LayoutInflater inflater;
    public static String cat_id;
    int selectedPosition;
    public static int page_no = 1;

    public Shop_subCategory_Adapter(Context context, List<Category_List_Model> product_model) {
        this.context = context;
        this.subcat_model = product_model;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedPosition=0;
    }

    @Override
    public Shop_subCategory_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.shop_categorylist_row, parent, false);
        return new Shop_subCategory_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Shop_subCategory_Adapter.MyViewHolder holder, final int position) {

        final Category_List_Model model = subcat_model.get(position);
       // String title = model.getCategory_title();
       // holder.tv_cat_title.setText(Html.fromHtml(title));
        Navigation_activity.Check_String_NULL_Value(holder.tv_cat_title, model.getCategory_title());
        if(selectedPosition==position)
        {
            holder.lv_shop_new_click.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.colorPrimaryDark));
            holder.tv_cat_title.setTextColor(context.getResources().getColor(R.color.white));

        }else {
            holder.lv_shop_new_click.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.white));
            holder.tv_cat_title.setTextColor(context.getResources().getColor(R.color.black));

        }
         /* if(selectedPosition==position)
          {
              holder.lv_shop_new_click.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.colorPrimaryDark));
              holder.tv_cat_title.setTextColor(context.getResources().getColor(R.color.white));

          }else {
              holder.lv_shop_new_click.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.white));
              holder.tv_cat_title.setTextColor(context.getResources().getColor(R.color.black));

          }*/

        holder.lv_shop_new_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   Toast.makeText(context, "click"+subcat_model.get(position).getCategory_title(), Toast.LENGTH_SHORT).show();
                Final_button_shop_activity.CALL_RETROFIT_SHOP_API(subcat_model.get(position).getCategory_id(), page_no,Main_Url.GRID_TYPE);
                mSelectedItem = position;
                notifyDataSetChanged();

            }
        });

        if (mSelectedItem == position) {
            holder.lv_shop_new_click.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.colorPrimaryDark));
            holder.tv_cat_title.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.lv_shop_new_click.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.white));
            holder.tv_cat_title.setTextColor(context.getResources().getColor(R.color.black));
        }

      /*  if(title.equals("Offers") || title=="Offers")
        {
            int offerPos=position;
            Log.e("offerPos",offerPos+"");
        }

        if(position==113)
        {
            cat_id = subcat_model.get(113).getSubcat_id();

            //  cat_id = subcat_model.get(position).getSubcat_id();
            Log.e("id_subcatid_position", "" + subcat_model.get(position).getSubcat_id());
            Log.e("id_adapter_position", "" + position);
            selectedPosition = position;

            Shop_Activity.CALL_SHOP_PRODUCT_API(cat_id,page_no);
            holder.tv_shop_name.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        }


        Log.e("adapter_pos",""+position);
        Log.e("getadapter_position",""+holder.getAdapterPosition());
//        Glide.with(context).load(model.getCat_image()).into(holder.iv_image);

        if(position==0)
        {
            Log.e("adapter_poszero",""+position);
            //Shop_Activity.CALL_SHOPALL_API(page_no);
            Log.e("view_type_65","list"+Shop_Activity.view_type);


            Shop_Activity.CALL_SHOPALL_API(page_no);


            Log.e("titlesub_in_else",""+model.getSubcat_title());
        }
        if(model.getSubcat_title().equals("Offers") && model.getSubcat_id().equals(230)){

        }


        holder.lv_shop_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_id = subcat_model.get(position).getSubcat_id();

              //  cat_id = subcat_model.get(position).getSubcat_id();
                Log.e("id_subcatid_position", "" + subcat_model.get(position).getSubcat_id());
                Log.e("id_adapter_position", "" + position);
                selectedPosition = position;

                Shop_Activity.CALL_SHOP_PRODUCT_API(cat_id,page_no);
                holder.tv_shop_name.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                    //   Home_frag.CALL_EXPLORED_PRODUCT_API(cat_id);

                //notifyDataSetChanged();
            }
        });
       if (selectedPosition == position) {
           Log.e("adapter_pos_selected",""+position);
            Login_preference.setcatid(context, cat_id);
            holder.view_shop.setVisibility(View.VISIBLE);
           holder.tv_shop_name.setTextColor(context.getResources().getColor(R.color.colorPrimary));

       } else {
            holder.view_shop.setVisibility(View.GONE);
           holder.tv_shop_name.setTextColor(context.getResources().getColor(R.color.black));


       }
*/
    }


    @Override
    public int getItemCount() {
        Log.e("modelsize", "" + subcat_model.size());
        return subcat_model.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cat_title;
        // public ImageView iv_image;
        LinearLayout lv_shop_new_click;
        // View view_shop;

        public MyViewHolder(View itemView) {
            super(itemView);

            // view_shop = (View) itemView.findViewById(R.id.view_shop);
            tv_cat_title = (TextView) itemView.findViewById(R.id.tv_cat_title);
            lv_shop_new_click = (LinearLayout) itemView.findViewById(R.id.lv_shop_new_click);
            // iv_image = (ImageView) itemView.findViewById(R.id.iv_image);
        }

    }


}







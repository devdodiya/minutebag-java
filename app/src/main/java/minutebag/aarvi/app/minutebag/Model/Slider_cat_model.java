package minutebag.aarvi.app.minutebag.Model;




public class Slider_cat_model {
    String banners_id,banners_image,type,banners_title;
    public Slider_cat_model(String banners_id, String banners_image, String type, String banners_title) {

        this.banners_id = banners_id;
        this.banners_image = banners_image;
        this.type = type;
        this.banners_title = banners_title;
    }

    public String getBanners_id() {
        return banners_id;
    }

    public void setBanners_id(String banners_id) {
        this.banners_id = banners_id;
    }

    public String getBanners_image() {
        return banners_image;
    }

    public void setBanners_image(String banners_image) {
        this.banners_image = banners_image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBanners_title() {
        return banners_title;
    }

    public void setBanners_title(String banners_title) {
        this.banners_title = banners_title;
    }

   }

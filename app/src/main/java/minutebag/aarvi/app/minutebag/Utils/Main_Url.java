package minutebag.aarvi.app.minutebag.Utils;

public class Main_Url {


    public static final int POST_PER_REQUEST = 10;
    public static long DELAY_TIME = 250;
    public static int WEBSITE_ID=1,POLICY_ID=2,REFUND_ID=3,TERMS_ID=4;
    public static int GRID_TYPE=0;
    //main url minutebag
    public static String sortUrl = "https://minutebag.com/wp-json/wc/v3/";

    //baneer home page
    public static String HOME_BANNERURL = "https://minutebag.com/api/appsettings/get_all_banners/";

    //category home
    public static String consumer_key = "ck_0785376043847cba9836a37ccde4002bec4fb5f0";
    public static String consumer_secret = "cs_e037ae395709ad316322afe573fec7383b05df5f";
    public static String per_page = "100", hide_empty = "true";
    public static String pass_per_page = "10";

    //https://minutebag.com/wp-json/wc/v3/products/categories?consumer_key=ck_0785376043847cba9836a37ccde4002bec4fb5f0&consumer_secret=cs_e037ae395709ad316322afe573fec7383b05df5f&page=1&per_page=10&hide_empty=true
    public static String CATEGORY_URL = sortUrl + "products/categories?consumer_key="
            + consumer_key + "&consumer_secret=" + consumer_secret;


    //product latest,on_sale,featured
    //https://minutebag.com/wp-json/wc/v3/products?consumer_key=ck_0785376043847cba9836a37ccde4002bec4fb5f0&consumer_secret=cs_e037ae395709ad316322afe573fec7383b05df5f&on_sale=true
    //featured,onsale,latest
    public static String PRODUCT_ONSALE_URL = sortUrl + "products?consumer_key=" + consumer_key +
            "&consumer_secret=" + consumer_secret;

    //////explore product
    public static String EXPLORE_PRODUCT = sortUrl + "products?consumer_key=" + consumer_key +
            "&consumer_secret=" + consumer_secret;

    //////explore product home
///https://minutebag.com/wp-json/wc/v3/products?
// consumer_key=ck_0785376043847cba9836a37ccde4002bec4fb5f0&consumer_secret
// =cs_e037ae395709ad316322afe573fec7383b05df5f&page=1&per_page=10&category=17
    public static String EXPLORE_HOME_PRODUCT = sortUrl + "products?consumer_key=" + consumer_key +
            "&consumer_secret=" + consumer_secret;


    //product detail
    //https://minutebag.com/wp-json/wc/v3/products/8035?consumer_key=ck_0785376043847cba9836a37ccde4002bec4fb5f0&consumer_secret=cs_e037ae395709ad316322afe573fec7383b05df5f

    public static String PRODUCT_DETAIL_URL = sortUrl + "products/";


    //Related product Item
    //https://minutebag.com/wp-json/wc/v3/products/?consumer_key=ck_0785376043847cba9836a37ccde4002bec4fb5f0&consumer_secret=cs_e037ae395709ad316322afe573fec7383b05df5f&include[]=7064&include[]=5587
    public  static  String RELATED_PRODUCT_ITEM_API=sortUrl+"products/?"+"consumer_key=" + consumer_key +
            "&consumer_secret=" + consumer_secret;

    // Login URL
    public static String LOGIN_URL = "https://minutebag.com/api/appusers/generate_cookie";

    //settings URl
    public static String SETTINGS_URL = "https://minutebag.com/api/appsettings/get_all_settings/";

    //https://minutebag.com/api/appusers/send_mail/?insecure=cool&email=test@gmail.com&name=test&message=testtesttest
    public static String CONTACT_US_URL = "https://minutebag.com/api/appusers/send_mail/?insecure=cool";

    //sign up url
    public static String SIGN_UP_URL=sortUrl+"customers?consumer_key="+consumer_key+ "&consumer_secret="+consumer_secret;

          public static String EDIT_URL=sortUrl + "customers/";
  /*  retrive customer based on id
    https://minutebag.com/wp-json/wc/v3/customers/314*/
}
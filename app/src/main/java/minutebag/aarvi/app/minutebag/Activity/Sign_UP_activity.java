package minutebag.aarvi.app.minutebag.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import minutebag.aarvi.app.minutebag.Adapter.Category_grid_adapter;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class Sign_UP_activity extends AppCompatActivity {

    EditText txtfirstname,txtpassword,txtlastname,txtusername,txtemail;
    TextView btnregister,txtsignpolicy;
    String SIGN_UP;
    String username,password,firstname,lastname,email;
    Context ctx=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign__up_activity);
        setTitle("Create an Account");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        allocatememory();
        setevent();
    }

    private void setevent() {
        btnregister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CALL_REGISTER_API();
            }
        });

        txtfirstname.addTextChangedListener(new CustomerTextWatcher());
        txtlastname.addTextChangedListener(new CustomerTextWatcher());
        txtusername.addTextChangedListener(new CustomerTextWatcher());
        txtpassword.addTextChangedListener(new CustomerTextWatcher());
        txtemail.addTextChangedListener(new CustomerTextWatcher());
        txtsignpolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(ctx, SettingView.class);
                intent.putExtra("set_id", Main_Url.POLICY_ID);
                startActivity(intent);
            }
        });
    }
    private void CALL_REGISTER_API() {
        SIGN_UP = Main_Url.SIGN_UP_URL;
        Log.e("sign_up_url", "" + SIGN_UP);
        //  enableProgressBar();
        username = txtusername.getText().toString().trim();
        password = txtpassword.getText().toString().trim();
        firstname = txtfirstname.getText().toString().trim();
        lastname = txtlastname.getText().toString().trim();
        email = txtemail.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Main_Url.SIGN_UP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_reg", "" + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            final String id = jsonObject.getString("id");

                            // Log.e("message", "" + message);
                            Toast.makeText(ctx, "Register successfully", Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(Sign_UP_activity.this,Login_activity.class);
                            startActivity(i);

                            //  String data = jsonObject.getString("data");
                            //JSONObject data_obj = new JSONObject(data);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        Log.e("errorobj",""+obj);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                Toast.makeText(Sign_UP_activity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> KeyValuePair = new HashMap<String, String>();
                Log.e("username",""+username);
                Log.e("firstname",""+firstname);
                Log.e("email",""+email);
                Log.e("password",""+password);
                Log.e("lastname",""+lastname);

                KeyValuePair.put("username", username);
                KeyValuePair.put("first_name",firstname);
                KeyValuePair.put("email", email);
                KeyValuePair.put("password",password);
                KeyValuePair.put("last_name", lastname);
                return KeyValuePair;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(Sign_UP_activity.this);
        requestQueue.add(stringRequest);



    }

    private void allocatememory()
    {
        txtemail=findViewById(R.id.txtemail);
        txtpassword=findViewById(R.id.txtpassword);
        txtusername=findViewById(R.id.txtusername);
        txtfirstname=findViewById(R.id.txtfirstname);
        txtlastname=findViewById(R.id.txtlastname);
        txtsignpolicy=findViewById(R.id.txtsignpolicy);
        btnregister=findViewById(R.id.btnregister);
        btnregister.setEnabled(false);
        btnregister.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    class CustomerTextWatcher implements TextWatcher
    {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable)
        {
            ValidateInput();
        }
    }

    private void ValidateInput()
    {
        username = txtusername.getText().toString().trim();
        password = txtpassword.getText().toString().trim();
        firstname = txtfirstname.getText().toString().trim();
        lastname = txtlastname.getText().toString().trim();
        email = txtemail.getText().toString().trim();
        if(username.length()>0 && password.length()>0 && firstname.length()>0 && lastname.length()>0 && email.length()>0)
        {
            btnregister.setEnabled(true);
            btnregister.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        else
        {
            btnregister.setEnabled(false);
            btnregister.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }
}

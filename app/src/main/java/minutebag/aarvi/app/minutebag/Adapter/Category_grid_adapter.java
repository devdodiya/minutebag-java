package minutebag.aarvi.app.minutebag.Adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import minutebag.aarvi.app.minutebag.Activity.Edit_Profile_activity;
import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;

import minutebag.aarvi.app.minutebag.Fragment.Sub_Category_freg;
import minutebag.aarvi.app.minutebag.Model.Category_Model;
import minutebag.aarvi.app.minutebag.R;

public class Category_grid_adapter extends BaseAdapter
    {
        private Context ctx;
        private ArrayList<Category_Model> gridmodel;
        LayoutInflater inflater ;
        public Category_grid_adapter(Context context,ArrayList<Category_Model> gridmodel)
        {
            this.ctx = context;
            this.gridmodel = gridmodel;
            inflater = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount()
        {
            return gridmodel.size();
        }

        @Override
        public Object getItem(int i)
        {
            return gridmodel.get(i);
        }

        @Override
        public long getItemId(int i)
        {
            return i;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup)
        {
            View CurrentGridView;
            CurrentGridView = view;
            final MyViewHolder holder;
            if(CurrentGridView==null)
            {
                holder = new MyViewHolder();
                CurrentGridView = inflater.inflate(R.layout.category_row,null);
                holder.iv_cat_img = (ImageView) CurrentGridView.findViewById(R.id.iv_cat_img);
                holder.lv_cat_click = (LinearLayout) CurrentGridView.findViewById(R.id.lv_cat_click);
                holder.tv_cat_title = (TextView) CurrentGridView.findViewById(R.id.tv_cat_title);
                CurrentGridView.setTag(holder);
            }
            else
            {
                holder = (MyViewHolder)CurrentGridView.getTag();
            }
            final Category_Model model = gridmodel.get(position);
           // holder.tv_cat_title.setText(Html.fromHtml(model.getCat_title()));

            Navigation_activity.Check_String_NULL_Value(holder.tv_cat_title,model.getCat_title());
            holder.lv_cat_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    Bundle id_bundle=new Bundle();
                    id_bundle.putString("cat_id",model.getCat_id());

                    Fragment myFragment = new Sub_Category_freg();
                    myFragment.setArguments(id_bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, myFragment).addToBackStack(null).commit();

                }
            });
          // Glide.with(ctx).load(model.getCat_image()).into(holder.iv_cat_img);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.drawable_ldpi_icon);
            requestOptions.error(R.drawable.drawable_ldpi_icon);
            Glide.with(ctx)
                    .setDefaultRequestOptions(requestOptions)
                    .load(model.getCat_image()).into(holder.iv_cat_img);
            Log.e("adaoter_home_position",""+position);
            return CurrentGridView;
        }
        class MyViewHolder {
            public ImageView iv_cat_img;
            LinearLayout lv_cat_click;
            TextView tv_cat_title;
        }
    }





package minutebag.aarvi.app.minutebag.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import minutebag.aarvi.app.minutebag.Activity.Navigation_activity;
import minutebag.aarvi.app.minutebag.Activity.Product_Detail;
import minutebag.aarvi.app.minutebag.Model.AddToCart;
import minutebag.aarvi.app.minutebag.Model.Shop_cat_model;
import minutebag.aarvi.app.minutebag.R;

import static minutebag.aarvi.app.minutebag.Adapter.Shop_list_adapter.cartList;

public class Explore_product_adapter extends RecyclerView.Adapter<Explore_product_adapter.MyViewHolder> {
    String old_price, new_price;
    int product_quantity = 0;
    public static CardView card_product;
    private List<Shop_cat_model> product_model;
    private Context context;
    LayoutInflater inflater;

    public Explore_product_adapter(Context context, List<Shop_cat_model> product_model) {
        this.context = context;
        this.product_model = product_model;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Explore_product_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.product_row, parent, false);
        return new Explore_product_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Explore_product_adapter.MyViewHolder holder, final int position) {

        final Shop_cat_model model = product_model.get(position);
        // holder.tv_product_title.setText(model.getProduct_name());
        Navigation_activity.Check_String_NULL_Value(holder.tv_product_title, model.getProduct_name());

        //  holder.tv_new_price.setText(context.getResources().getString(R.string.rs) + model.getProduct_new_price());
        //holder.tv_old_price.setText(context.getResources().getString(R.string.rs) + model.getProduct_old_price());

        Navigation_activity.Check_String_NULL_Value(holder.tv_new_price, context.getResources().getString(R.string.rs) + model.getProduct_new_price());
        Navigation_activity.Check_String_NULL_Value(holder.tv_old_price, context.getResources().getString(R.string.rs) + model.getProduct_old_price());
        holder.tv_old_price.setPaintFlags(holder.tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        double oldprice, newprice;
        //Log.e("old_price=",""+model.getProduct_old_price());
        //Log.e("new_price=",""+model.getProduct_new_price());

        old_price = model.getProduct_old_price();
        new_price = model.getProduct_new_price();
        if (old_price.equals("") == true) {
            old_price = "0";
        }
        if (new_price.equals("") == true) {
            new_price = "0";
        }

        /*  if(model.getProduct_old_price().equals("")!=true || model.getProduct_new_price().equals("")!=true){
         */
        oldprice = Double.parseDouble(old_price);
        newprice = Double.parseDouble(new_price);
        double save_rs = oldprice - newprice;
        int res = (int) save_rs;
        String result = String.valueOf(res);
        holder.tv_save_rs.setText("SAVE RS." + result);
      /*  }else{
            holder.tv_save_rs.setVisibility(View.GONE);
        }*/

        holder.lv_variable_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            /*    dialog.setContentView(R.layout.custom_dialog_shop);
                dialog.show();*/
                Dialog dialog = new Dialog(context);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                Rect displayRectangle = new Rect();
                Window window = activity.getWindow();
                window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

// inflate and adjust layout
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.custom_dialog_shop, null);
                layout.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
                layout.setMinimumHeight((int) (displayRectangle.height() * 0.4f));
                dialog.setContentView(layout);
                dialog.show();
            }
        });
        holder.lv_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.lv_add_to_cart.setVisibility(View.GONE);
                model.setIncrease(1);
                String total= String.valueOf(model.getIncrease() * Double.parseDouble(model.getProduct_new_price()));
                final AddToCart cartItem=new AddToCart(model.getProduct_old_price(),
                        model.getProduct_new_price(),
                        model.getProduct_name(),
                        model.getProduct_img(),
                        model.getIncrease(),
                        model.getProduct_id(),
                        total);
                AddToCart selectedCart=SearchItemUsingId(cartList,model.getProduct_id());
                if (selectedCart!=null)
                {
                    Shop_list_adapter.cartList.add(cartItem);
                }
                else {
                    selectedCart.setIncrease(Integer.parseInt(holder.tv_product_quantity.getText().toString())) ;
                    total= String.valueOf(selectedCart.getIncrease()*Double.parseDouble( selectedCart.getNewPrice()));
                    selectedCart.setTotal(total);
                }
                holder.lv_add_quantity.setVisibility(View.VISIBLE);
                holder.lv_increse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        model.setIncrease(model.getIncrease() + 1);
                        Log.e("product_104", "" + model.getIncrease());
                        String result = String.valueOf(model.getIncrease());
                        cartItem.setIncrease(model.getIncrease());
                            String total= String.valueOf(model.getIncrease()*Double.parseDouble( model.getProduct_new_price()));

                        cartItem.setTotal(total);
                        //Log.e("product_104", "" + product_quantity);
                        holder.tv_product_quantity.setText(result);
                    }
                });
                holder.lv_decrese.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (model.getIncrease() != 0) {
                            model.setIncrease(model.getIncrease() - 1);
                            String result = String.valueOf(model.getIncrease());
                            Log.e("product_1011_minus", "" + model.getIncrease());
                            cartItem.setIncrease(model.getIncrease());
                            String total= String.valueOf(model.getIncrease()*Double.parseDouble( model.getProduct_new_price()));

                            cartItem.setTotal(total);
                            holder.tv_product_quantity.setText(result);
                        } else {
                            holder.tv_product_quantity.setText(String.valueOf(product_quantity));
                        }
                    }
                });
            }
        });

        holder.lv_product_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                //Bundle id_bundle=new Bundle();
                //id_bundle.putString("cat_id",model.getCat_id());
                //Fragment myFragment = new Sub_Category_freg();
                // myFragment.setArguments(id_bundle);
                Intent i = new Intent(context, Product_Detail.class);
                //    Log.e("product_id_12", "" +  model.getProduct_id());

                i.putExtra("product_id", model.getProduct_id());
                activity.startActivity(i);

                // activity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, myFragment).addToBackStack(null).commit();


            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.drawable_ldpi_icon);
        requestOptions.error(R.drawable.drawable_ldpi_icon);
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(model.getProduct_img()).into(holder.iv_product);


        //Glide.with(context).load(model.getProduct_img()).into(holder.iv_product);


/*
            holder.ll_cat_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AppCompatActivity activity = (AppCompatActivity) view.getContext();

                            cat_id=category_model.get(position).getCategory_id();
                            Log.e("id",""+cat_id);
                            cat_name=category_model.get(position).getTxtcatnm();
                            Log.e("catname",""+cat_name);
                            Intent i=new Intent(context, Sub_category.class);
                            i.putExtra("cat_id",cat_id);
                            i.putExtra("catname",cat_name);
                            activity.startActivity(i);
                            activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                        }
                    }, 50);



                }
            });
*/
    }

    private AddToCart SearchItemUsingId(ArrayList<AddToCart> cartList, String product_id) {
        AddToCart selectedCart=null;
        for(AddToCart cart : cartList) {

            if(cart.getId().equals(product_id)){
                //found it!
                selectedCart=cart;
            }
        }
        return selectedCart;
    }


    @Override
    public int getItemCount() {
        return product_model.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_product, iv_wishlist;

        LinearLayout lv_product_click, lv_add_to_cart, lv_variable_spinner, lv_decrese, lv_increse, lv_add_quantity;
        TextView tv_save_rs, tv_product_title, tv_add_to_cart, tv_old_price, tv_new_price, tv_product_quantity, tv_minus, tv_add;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_product = (ImageView) itemView.findViewById(R.id.iv_product);
            iv_wishlist = (ImageView) itemView.findViewById(R.id.iv_wishlist);
            card_product = (CardView) itemView.findViewById(R.id.card_product);

            lv_product_click = (LinearLayout) itemView.findViewById(R.id.lv_product_click);
            lv_add_to_cart = (LinearLayout) itemView.findViewById(R.id.lv_add_to_cart);
            lv_decrese = (LinearLayout) itemView.findViewById(R.id.lv_decrese);
            lv_increse = (LinearLayout) itemView.findViewById(R.id.lv_increse);
            lv_add_quantity = (LinearLayout) itemView.findViewById(R.id.lv_add_quantity);
            lv_variable_spinner = (LinearLayout) itemView.findViewById(R.id.lv_variable_spinner);

            tv_add = (TextView) itemView.findViewById(R.id.tv_add);
            tv_minus = (TextView) itemView.findViewById(R.id.tv_minus);
            tv_product_quantity = (TextView) itemView.findViewById(R.id.tv_product_quantity);
            tv_save_rs = (TextView) itemView.findViewById(R.id.tv_save_rss);
            tv_product_title = (TextView) itemView.findViewById(R.id.tv_product_title);
            tv_old_price = (TextView) itemView.findViewById(R.id.tv_old_price);
            tv_new_price = (TextView) itemView.findViewById(R.id.tv_new_price);
            tv_add_to_cart = (TextView) itemView.findViewById(R.id.tv_add_to_cart);
        }
    }
}





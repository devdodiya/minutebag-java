package minutebag.aarvi.app.minutebag.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import minutebag.aarvi.app.minutebag.Fragment.Home_frag;
import minutebag.aarvi.app.minutebag.Preference.Login_preference;
import minutebag.aarvi.app.minutebag.R;
import minutebag.aarvi.app.minutebag.Utils.Main_Url;

public class Login_activity extends AppCompatActivity {
    LinearLayout lv_register;
    Context ctx=this;
    EditText txtusernamelogin,txtpasswordlogin;
    TextView btnlogin,loginerror;
    String username,password;
    String LOGI_URL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);
        allocatememory();
        setevent();
        setTitle("Login");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    private void setevent() {
        lv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login_activity.this,Sign_UP_activity.class));
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                username=txtusernamelogin.getText().toString().trim();
                password=txtpasswordlogin.getText().toString().trim();

                LOGI_URL = Main_Url.LOGIN_URL + "?username=" + username + "&password=" + password;
                Log.e("log_url", "" + LOGI_URL);


                StringRequest request=new StringRequest(StringRequest.Method.GET, LOGI_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("response_login",response);
                        try {

                            JSONObject jsonObject=new JSONObject(response);
                            String status=jsonObject.getString("status");
                            if(status.equals("ok"))
                            {
                                JSONObject user_object=jsonObject.getJSONObject("user");
                                String id=user_object.getString("id");
                                String first_name=user_object.getString("first_name");
                                String last_name=user_object.getString("last_name");
                                String email=user_object.getString("email");
                                Login_preference.setcustomer_id(ctx,id);
                                Login_preference.setfirstname(ctx,first_name);
                                Login_preference.setLastname(ctx,last_name);
                                Login_preference.setEmailid(ctx,email);
                                Toast.makeText(ctx, "Login Successfully", Toast.LENGTH_LONG).show();

                                startActivity(new Intent(ctx,Navigation_activity.class));
                                finish();
                            /*    Home_frag fragment = new CreateNewNote();
                                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                transaction.replace(R.id.container_layout, fragment).commit();
                            */
                            }
                            else {
                                /*{"status":"error","error":"Invalid username or password."}*/
                                String error=jsonObject.getString("error");
                                loginerror.setVisibility(View.VISIBLE);
                                loginerror.setText(error);
                                Toast.makeText(ctx, error, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error",error.toString());
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(ctx);
                requestQueue.add(request);
            }
        });
        txtusernamelogin.addTextChangedListener(new CustomerTextWatcher());
        txtpasswordlogin.addTextChangedListener(new CustomerTextWatcher());
    }

    private void allocatememory()
    {

        txtusernamelogin=findViewById(R.id.txtusernamelogin);
        txtpasswordlogin=findViewById(R.id.txtpasswordlogin);
        btnlogin=findViewById(R.id.btnlogin);
        loginerror=findViewById(R.id.loginerror);
        lv_register=findViewById(R.id.lv_register);
        btnlogin.setEnabled(false);
        btnlogin.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    class CustomerTextWatcher implements TextWatcher
    {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable)
        {
            ValidateInput();
        }
    }

    private void ValidateInput()
    {
        if(txtusernamelogin.getText().toString().trim().length()>0
                && txtpasswordlogin.getText().toString().trim().length()>0)
        {
            btnlogin.setEnabled(true);
            btnlogin.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        else
        {
            btnlogin.setEnabled(false);
            btnlogin.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }
}

package minutebag.aarvi.app.minutebag.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import minutebag.aarvi.app.minutebag.R;

public class Splash_Screen extends AppCompatActivity {
    ProgressBar pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);

        getSupportActionBar().hide();
        getApplicationContext().setTheme(android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        pd=(ProgressBar)findViewById(R.id.progressBar);
        Thread th=new Thread()
        {
            public  void run()
            {
                try
                {
                    pd.setVisibility(View.VISIBLE);
                    sleep(1500);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {

                    startActivity(new Intent(Splash_Screen.this,Navigation_activity.class));
                    finish();

                }
            }

        };
        th.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
